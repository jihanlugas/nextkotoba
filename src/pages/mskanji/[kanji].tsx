import Head from 'next/head';
import Main from '@com/Layout/Main';
import { BiChevronRight } from 'react-icons/bi';
import Link from 'next/link';
import { useEffect, useState, Fragment, useContext } from 'react';
import { Api } from '../../lib/Api';
import { useQuery, useMutation } from 'react-query';
import { AiOutlineLoading3Quarters, AiOutlinePlus, AiOutlineMinus } from 'react-icons/ai';
import { FaTimes } from 'react-icons/fa';
import { isEmptyObject } from '@utils/Validate';
import { Form, Formik, FormikValues, FieldArray, FormikErrors } from 'formik';
import TextField from '@com/Formik/TextField';
import TextAreaField from '@com/Formik/TextAreaField';
import ButtonSubmit from '@com/Formik/ButtonSubmit';
import * as Yup from 'yup';
import NotifContext from '@stores/notifContext';
import { GetServerSideProps } from 'next';
import PageWithLayoutType from '@type/layout';
import UserContext from '@stores/userProvider';


interface ImskanjiExample {
	mskanjiexampleId?: number,
	mskanjiId?: number,
	kanjiexample?: string,
	kana?: string,
	mean?: string,
	description?: string
}

interface Ikanji {
	mskanjiId?: number,
	kanji?: string,
	grade?: string,
	jlpt?: string,
	unicode?: string,
	heisigEn?: string,
	strokeCount?: string,
	listMeaning?: string[],
	listKunReading?: string[],
	listOnReading?: string[],
	listNameReading?: string[],
	listMskanjiExample?: ImskanjiExample[],
}

interface Props {
	success: boolean,
	error: boolean,
	payload?: Ikanji,
	message: string,
}


let schema = Yup.object().shape({
	mskanjiId: Yup.number().required(),
	kanji: Yup.string().required(),
	grade: Yup.string().oneOf(['0', '1', '2', '3', '4', '5']),
	jlpt: Yup.string().oneOf(['0', '1', '2', '3', '4', '5']),
	unicode: Yup.string(),
	heisigEn: Yup.string(),
	strokeCount: Yup.number().required(),
	listMeaning: Yup.array().of(Yup.string().required('meaning is a required field')),
	listKunReading: Yup.array().of(Yup.string().matches(/^[ぁ-ん.。\s|-]+$/, 'field hiragana').required('kun reading is a required field')),
	listOnReading: Yup.array().of(Yup.string().matches(/^[ァ-ン.。\s|-]+$/, 'field katakana').required('on reading is a required field')),
	listNameReading: Yup.array().of(Yup.string().matches(/^[ぁ-んァ-ン.。\s|-]+$/, 'field kana').required('name reading is a required field')),
	listMskanjiExample: Yup.array().of(
		Yup.object().shape({
			mskanjiexampleId: Yup.number(),
			mskanjiId: Yup.number(),
			kanjiexample: Yup.string().matches(/^[一-龯ぁ-んァ-ン.。\s|-]+$/, 'field kana').required('kanji example is a required field'),
			kana: Yup.string().matches(/^[ぁ-んァ-ン.。\s|-]+$/, 'field kana').required('kana is a required field'),
			mean: Yup.string().required('mean is a required field'),
			description: Yup.string().required('description is a required field'),
		})
	)
});


const Kanji: React.FC<Props> = ({ error, success, message, payload }) => {
	const { notif } = useContext(NotifContext);
	const { user, setUser } = useContext(UserContext);
	const { data: dataSubmit, mutate: mutateSubmit, isLoading: isLoadingSubmit } = useMutation((val: FormikValues) => Api.post('/mskanji/update', val));

	const kanji = payload;
	const [defaultMskanjiexample, sefaultMskanjiexample] = useState<ImskanjiExample>({});
	const [accordionMeaning, setAccordionMeaning] = useState<boolean>(true);
	const [accordionKunReading, setAccordionKunReading] = useState<boolean>(true);
	const [accordionOnReading, setAccordionOnReading] = useState<boolean>(true);
	const [accordionNameReading, setAccordionNameReading] = useState<boolean>(true);
	const [accordionMskanjiExample, setAccordionMskanjiExample] = useState<boolean>(true);

	const handleSubmit = (values: FormikValues, setErrors) => {
		mutateSubmit(values, {
			onSuccess: (res) => {
				if (res) {
					if (res.success) {
						notif.success(res.message);
					} else if (res.error) {
						if (res.payload && res.payload.listError) {
							setErrors(res.payload.listError);
						} else {
							notif.error(res.message);
						}
					}
				}
			},
			onError: (res) => {
				notif.error('Please cek you connection');
			},
		});
	};


	return (
		<>
			<Head>
				<title>{'Master Kanji - ' + kanji.kanji}</title>
				<meta name="og:title" content={'Master Kanji | Master Kanji - ' + kanji.kanji + ' | Belajar Bahasa Jepang Online | kotoba'} />
				<meta name="og:description" content="..." />
			</Head>
			<div className={'p-4 font-sans w-full max-w-4xl mx-auto'}>
				<div className={'mb-4 flex items-center'}>
					<Link href={{ pathname: '/mskanji', query: { kanji: kanji.kanji } }}>
						<a>
							<h1 className={'mr-2'}>
								{'Master Kanji'}
							</h1>
						</a>
					</Link>
					<BiChevronRight className={'mr-2'} size={'1em'} />
					<h1 className={'mr-2'} >{kanji.kanji}</h1>
				</div>
				{user === null ? (
					<div className={'mb-4'}>
						<h1 className={'text-base'}>
							Please Login to access this page
						</h1>
					</div>
				) : !user.userId ? (
					<div className={'mb-4'}>
					</div>
				) : (
					<>
						<div className={'mb-4'}>
							<Formik
								initialValues={kanji}
								validationSchema={schema}
								enableReinitialize={true}
								onSubmit={(values, { setErrors }) => handleSubmit(values, setErrors)}
							>
								{({ values, errors }) => {
									return (
										<Form>
											<div className="flex justify-between">
												<div className="shrink-0 h-20 w-20 sm:h-28 sm:w-28 mb-4">
													<div className={'text-5xl sm:text-6xl font-yujiSyuku select-none h-full w-full flex justify-center items-center bg-gray-50 rounded-lg shadow-md'}>
														{values.kanji}
													</div>
												</div>
												<div className={'w-24'}>
													<ButtonSubmit
														label={'Save'}
														disabled={isLoadingSubmit}
														loading={isLoadingSubmit}
													/>
												</div>
											</div>
											<div>
												<div className="mb-4">
													<TextField
														label={'Grade'}
														name={'grade'}
														type={'text'}
														placeholder={'Grade'}
													/>
												</div>
												<div className="mb-4">
													<TextField
														label={'JLPT Level'}
														name={'jlpt'}
														type={'text'}
														placeholder={'JLPT'}
													/>
												</div>
												<div className="mb-4">
													<TextField
														label={'Unicode'}
														name={'unicode'}
														type={'text'}
														placeholder={'Unicode'}
													/>
												</div>
												<div className="mb-4">
													<TextField
														label={'Heisig En'}
														name={'heisigEn'}
														type={'text'}
														placeholder={'Heisig En'}
													/>
												</div>
												<div className="mb-4">
													<TextField
														label={'Stroke Count'}
														name={'strokeCount'}
														type={'text'}
														placeholder={'Stroke Count'}
													/>
												</div>
												<div>
													<FieldArray
														name={'listMeaning'}
														render={arrayHelpers => (
															<div className={'mb-4 border-2 rounded overflow-hidden'}>
																<div className="flex items-center justify-between px-2 bg-gray-200 hover:bg-gray-300" onClick={() => setAccordionMeaning(!accordionMeaning)}>
																	<div >
																		List Meaning
																	</div>
																	<div className={'h-10 w-10 flex justify-center items-center'}>
																		<AiOutlinePlus className={`transform transition duration-300 ease-in-out ${accordionMeaning && 'rotate-45'}`} size={'1em'} />
																	</div>
																</div>
																<div className={`${accordionMeaning ? 'h-full' : 'h-0'}`}>
																	<div className={'pt-4 px-2'}>
																		{values.listMeaning && values.listMeaning.map((meaning, key) => {
																			return (
																				<div key={key}>
																					<div className="mb-4 flex">
																						<TextField
																							name={`listMeaning.${key}`}
																							type={'text'}
																							placeholder={'Meaning'}
																						/>
																						<div className={'h-10 w-10 flex justify-center items-center'} onClick={() => arrayHelpers.remove(key)}>
																							<FaTimes className={'text-red-500'} size={'1em'} />
																						</div>
																					</div>
																				</div>
																			);
																		})}
																	</div>
																	<div className={'flex justify-end px-2 mb-2'}>
																		<div className={'flex items-center'} onClick={() => arrayHelpers.push('')}>
																			<AiOutlinePlus className={'mr-2'} size={'1em'} />
																			<div>Add Meaning</div>
																		</div>
																	</div>
																</div>
															</div>
														)}
													/>
												</div>
												<div>
													<FieldArray
														name={'listKunReading'}
														render={arrayHelpers => (
															<div className={'mb-4 border-2 rounded overflow-hidden'}>
																<div className="flex items-center justify-between px-2 bg-gray-200 hover:bg-gray-300" onClick={() => setAccordionKunReading(!accordionKunReading)}>
																	<div >
																		List Kun Reading
																	</div>
																	<div className={'h-10 w-10 flex justify-center items-center'}>
																		<AiOutlinePlus className={`transform transition duration-300 ease-in-out ${accordionKunReading && 'rotate-45'}`} size={'1em'} />
																	</div>
																</div>
																<div className={`${accordionKunReading ? 'h-full' : 'h-0'}`}>
																	<div className={'pt-4 px-2'}>
																		{values.listKunReading && values.listKunReading.map((kunReading, key) => {
																			return (
																				<div key={key}>
																					<div className="mb-4 flex">
																						<TextField
																							name={`listKunReading.${key}`}
																							type={'text'}
																							placeholder={'Kun Reading'}
																						/>
																						<div className={'h-10 w-10 flex justify-center items-center'} onClick={() => arrayHelpers.remove(key)}>
																							<FaTimes className={'text-red-500'} size={'1em'} />
																						</div>
																					</div>
																				</div>
																			);
																		})}
																	</div>
																	<div className={'flex justify-end px-2 mb-2'}>
																		<div className={'flex items-center'} onClick={() => arrayHelpers.push('')}>
																			<AiOutlinePlus className={'mr-2'} size={'1em'} />
																			<div>Add Kun Reading</div>
																		</div>
																	</div>
																</div>
															</div>
														)}
													/>
												</div>
												<div>
													<FieldArray
														name={'listOnReading'}
														render={arrayHelpers => (
															<div className={'mb-4 border-2 rounded overflow-hidden'}>
																<div className="flex items-center justify-between px-2 bg-gray-200 hover:bg-gray-300" onClick={() => setAccordionOnReading(!accordionOnReading)}>
																	<div >
																		List On Reading
																	</div>
																	<div className={'h-10 w-10 flex justify-center items-center'}>
																		<AiOutlinePlus className={`transform transition duration-300 ease-in-out ${accordionOnReading && 'rotate-45'}`} size={'1em'} />
																	</div>
																</div>
																<div className={`${accordionOnReading ? 'h-full' : 'h-0'}`}>
																	<div className={'pt-4 px-2'}>
																		{values.listOnReading && values.listOnReading.map((onReading, key) => {
																			return (
																				<div key={key}>
																					<div className="mb-4 flex">
																						<TextField
																							name={`listOnReading.${key}`}
																							type={'text'}
																							placeholder={'On Reading'}
																						/>
																						<div className={'h-10 w-10 flex justify-center items-center'} onClick={() => arrayHelpers.remove(key)}>
																							<FaTimes className={'text-red-500'} size={'1em'} />
																						</div>
																					</div>
																				</div>
																			);
																		})}
																	</div>
																	<div className={'flex justify-end px-2 mb-2'}>
																		<div className={'flex items-center'} onClick={() => arrayHelpers.push('')}>
																			<AiOutlinePlus className={'mr-2'} size={'1em'} />
																			<div>Add On Reading</div>
																		</div>
																	</div>
																</div>
															</div>
														)}
													/>
												</div>
												<div>
													<FieldArray
														name={'listNameReading'}
														render={arrayHelpers => (
															<div className={'mb-4 border-2 rounded overflow-hidden'}>
																<div className="flex items-center justify-between px-2 bg-gray-200 hover:bg-gray-300" onClick={() => setAccordionNameReading(!accordionNameReading)}>
																	<div >
																		List Name Reading
																	</div>
																	<div className={'h-10 w-10 flex justify-center items-center'}>
																		<AiOutlinePlus className={`transform transition duration-300 ease-in-out ${accordionNameReading && 'rotate-45'}`} size={'1em'} />
																	</div>
																</div>
																<div className={`${accordionNameReading ? 'h-full' : 'h-0'}`}>
																	<div className={'pt-4 px-2'}>
																		{values.listNameReading && values.listNameReading.map((nameReading, key) => {
																			return (
																				<div key={key}>
																					<div className="mb-4 flex">
																						<TextField
																							name={`listNameReading.${key}`}
																							type={'text'}
																							placeholder={'Name Reading'}
																						/>
																						<div className={'h-10 w-10 flex justify-center items-center'} onClick={() => arrayHelpers.remove(key)}>
																							<FaTimes className={'text-red-500'} size={'1em'} />
																						</div>
																					</div>
																				</div>
																			);
																		})}
																	</div>
																	<div className={'flex justify-end px-2 mb-2'}>
																		<div className={'flex items-center'} onClick={() => arrayHelpers.push('')}>
																			<AiOutlinePlus className={'mr-2'} size={'1em'} />
																			<div>Add Name Reading</div>
																		</div>
																	</div>
																</div>
															</div>
														)}
													/>
												</div>
												<div>
													<FieldArray
														name={'listMskanjiExample'}
														render={arrayHelpers => (
															<div className={'mb-4 border-2 rounded overflow-hidden'}>
																<div className="flex items-center justify-between px-2 bg-gray-200 hover:bg-gray-300" onClick={() => setAccordionMskanjiExample(!accordionMskanjiExample)}>
																	<div >
																		List Master Kanji Example
																	</div>
																	<div className={'h-10 w-10 flex justify-center items-center'}>
																		<AiOutlinePlus className={`transform transition duration-300 ease-in-out ${accordionMskanjiExample && 'rotate-45'}`} size={'1em'} />
																	</div>
																</div>
																<div className={`${accordionMskanjiExample ? 'h-full' : 'h-0'}`}>
																	<div className={'pt-4 px-2'}>
																		{values.listMskanjiExample && values.listMskanjiExample.map((mskanjiExample, key) => {
																			return (
																				<div key={key}>
																					<div className="flex justify-between items-center">
																						<div>
																							Master Kanji Example
																						</div>
																						<div className={'h-10 w-10 flex justify-center items-center'} onClick={() => arrayHelpers.remove(key)}>
																							<FaTimes className={'text-red-500'} size={'1em'} />
																						</div>
																					</div>
																					<div className="mb-4">
																						<TextField
																							label={'Kanji Example'}
																							name={`listMskanjiExample.${key}.kanjiexample`}
																							type={'text'}
																							placeholder={'Kanji Example'}
																						/>
																					</div>
																					<div className="mb-4">
																						<TextField
																							label={'Kana'}
																							name={`listMskanjiExample.${key}.kana`}
																							type={'text'}
																							placeholder={'Kana'}
																						/>
																					</div>
																					<div className="mb-4">
																						<TextField
																							label={'Mean'}
																							name={`listMskanjiExample.${key}.mean`}
																							type={'text'}
																							placeholder={'Mean'}
																						/>
																					</div>
																					<div className="mb-4">
																						<TextAreaField
																							label={'Description'}
																							name={`listMskanjiExample.${key}.description`}
																							type={'text'}
																							placeholder={'Description'}
																						/>
																					</div>
																				</div>
																			);
																		})}
																	</div>
																	<div className={'flex justify-end px-2 mb-2'}>
																		<div className={'flex items-center'} onClick={() => arrayHelpers.push(defaultMskanjiexample)}>
																			<AiOutlinePlus className={'mr-2'} size={'1em'} />
																			<div>Add Master Kanji Example</div>
																		</div>
																	</div>
																</div>
															</div>
														)}
													/>
												</div>
											</div>
										</Form>
									);
								}}
							</Formik>
						</div>
					</>
				)}
			</div>
		</>
	);
};

(Kanji as PageWithLayoutType).layout = Main;

export const getServerSideProps: GetServerSideProps = async (context) => {
	const { kanji } = context.query;

	const data = await Api.get('/mskanji/kanji/' + kanji).then(res => res);

	return {
		props: data
	};
};

export default Kanji;