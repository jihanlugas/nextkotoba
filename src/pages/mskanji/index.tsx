import Main from '@com/Layout/Main';
import Head from 'next/head';
import Link from 'next/link';
import type { NextPage, NextPageContext } from 'next';
import React, { useState, ChangeEvent, useEffect, useContext } from 'react';
import { useDebounce } from '@utils/Hooks';
import { isEmptyObject } from '@utils/Validate';
import { Api } from '@lib/Api';
import { useMutation } from 'react-query';
import NotifContext from '@stores/notifContext';
import { AiOutlineLoading3Quarters } from 'react-icons/ai';
import { RiPencilLine } from 'react-icons/ri';
import { useRouter } from 'next/router';
import { getRandomKanji } from '@utils/Helper';
import { GetServerSideProps } from 'next';
import PageWithLayoutType from '@type/layout';
import Example from '@com/View/Example';
import OnReading from '@com/View/OnReading';
import KunReading from '@com/View/KunReading';
import NameReading from '@com/View/NameReading';
import UserContext from '@stores/userProvider';

interface Props {
	qKanji?: string
}

interface ImskanjiExample {
	mskanjiexampleId?: number,
	mskanjiId?: number,
	kanjiexample?: string,
	kana?: string,
	mean?: string,
	description?: string
}

interface Imskanji {
	mskanjiId?: number,
	kanji?: string,
	grade?: string,
	jlpt?: string,
	unicode?: string,
	heisigEn?: string,
	strokeCount?: string,
	listMeaning?: string[],
	listKunReading?: string[],
	listOnReading?: string[],
	listNameReading?: string[],
	listMskanjiExample?: ImskanjiExample[],
}

const Mskanji: React.FC<Props> = ({ qKanji = '' }) => {
	const { notif } = useContext(NotifContext);
	const { user, setUser } = useContext(UserContext);
	const [search, setSearch] = useState<string>(qKanji);
	const [randomKanji, setRandomKanji] = useState<string>(getRandomKanji());
	const debounceSearch = useDebounce(search, 1000);
	const [kanji, setKanji] = useState<Imskanji>({});
	const router = useRouter();

	const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
		setSearch(event.target.value);
	};

	const handlePressExample = (kanji: string) => {
		setSearch(kanji);
	};

	const { data: dataKanji, mutate: mutateKanji, isLoading } = useMutation((val: { kanji: string }) => Api.get('/mskanji/search', val));


	useEffect(() => {
		setRandomKanji(getRandomKanji());
		if (dataKanji && dataKanji.success) {
			setKanji(dataKanji.payload);
		} else {
			if ((dataKanji && dataKanji.error)) {
				// notif.error(dataKanji.message)
			}
			setKanji({});
		}
	}, [dataKanji]);

	useEffect(() => {
		if (debounceSearch === '') {
			router.push({ pathname: '/mskanji' }, undefined, { shallow: true });
			setKanji({});
		} else {
			router.push({ pathname: '/mskanji', query: { kanji: debounceSearch } }, undefined, { shallow: true });
			mutateKanji({ kanji: debounceSearch });
		}
	}, [debounceSearch]);

	return (
		<>
			<Head>
				<title>{'Master Kanji'}</title>
				<meta name="og:title" content="Master Kanji | Master Kanji | Belajar Bahasa Jepang Online | kotoba" />
				<meta name="og:description" content="..." />
			</Head>
			<div className={'p-4 font-sans w-full max-w-4xl mx-auto'}>
				<div className={''}>
					<div className={'flex justify-between items-center'}>
						<h1 className={''} >
							{'Master Kanji'}
						</h1>
						{user && user.userId !== 0 && (
							<>
								{!isEmptyObject(kanji) && (
									<Link href={`/mskanji/${kanji.kanji}`} >
										<a>
											<RiPencilLine className={''} size={'1.5em'} />
										</a>
									</Link>
								)}
							</>
						)}
					</div>
				</div>
				{user === null ? (
					<div className={'mb-4'}>
						<h1 className={'text-base'}>
							Please Login to access this page
						</h1>
					</div>
				) : !user.userId ? (
					<div className={'mb-4'}>
					</div>
				) : (
					<>
						<div className={'mb-4'}>
							<input
								type="text"
								value={search}
								onChange={handleChange}
								className={'w-full border-2 rounded h-10 px-2 bg-gray-50 font-notoSerif'}
								placeholder={'Search a kanji'}
							/>
						</div>
						<div className={'mb-4'}>
							{isLoading ? (
								<div className={'w-full h-60 flex justify-center items-center'}>
									<AiOutlineLoading3Quarters className={'animate-spin font-bold'} size={'4em'} />
								</div>
							) : (
								<div>
									{isEmptyObject(kanji) && debounceSearch === '' ? (
										<div>
											<div>To do search</div>
											<div className={'flex'}>
												<div className={'text-blue-600 cursor-pointer flex items-center'} onClick={() => handlePressExample(randomKanji)} >
													<span className={'mr-2'}>{'Example :'}</span>
													<span className={'font-notoSerif'}>{randomKanji}</span>
												</div>
											</div>
										</div>
									) : isEmptyObject(kanji) && debounceSearch !== '' ? (
										<div className={'w-full h-40 flex justify-center items-center'}>
											Data Not Found
										</div>
									) : (
										<div>
											<div className={'flex mb-4'}>
												<div className="flex-shrink-0 h-20 w-20 sm:h-28 sm:w-28">
													<div className={'text-5xl sm:text-6xl font-yujiSyuku select-none h-full w-full flex justify-center items-center bg-gray-50 rounded-lg shadow-md'}>
														{kanji.kanji}
													</div>
												</div>
												<div className="flex-1 h-20 grid grid-cols-3 gap-1 sm:h-28">
													<div className={'flex justify-center items-center flex-col'}>
														<div className={'font-semibold text-lg'}>{kanji.jlpt}</div>
														<div className={'font-light text-sm'}>Jlpt</div>
													</div>
													<div className={'flex justify-center items-center flex-col'}>
														<div className={'font-semibold text-lg'}>{kanji.grade}</div>
														<div className={'font-light text-sm'}>Grade</div>
													</div>
													<div className={'flex justify-center items-center flex-col'}>
														<div className={'font-semibold text-lg'}>{kanji.strokeCount}</div>
														<div className={'font-light text-sm'}>Stroke</div>
													</div>
												</div>
											</div>
											<div className={'mb-2 border-b-2'}>
												<div className={'mb-2'}>
													<div className={'text-lg'}>
														Heisig En
													</div>
													<div className={'capitalize'}>{kanji.heisigEn}</div>
												</div>
											</div>

											<div className={'mb-2 border-b-2'}>
												<div className={'mb-2'}>
													<div className={'text-lg'}>
														Meaning
													</div>
													{kanji.listMeaning.map((meaning, key) => {
														return (
															<div key={key} className={'capitalize'}>
																{(key + 1) + '. ' + meaning}
															</div>
														);
													})}
												</div>
											</div>
											<div className={'mb-2 border-b-2'}>
												<div className={'mb-2'}>
													<div className={'text-lg mb-2'}>
														On Readings
													</div>
													<div className={'flex flex-wrap'}>
														{kanji.listOnReading.length !== 0 ? kanji.listOnReading.map((onReading, key) => {
															return (
																<OnReading key={key} data={onReading} />
															);
														}) : <div>-</div>}
													</div>
												</div>
											</div>
											<div className={'mb-2 border-b-2'}>
												<div className={'mb-2'}>
													<div className={'text-lg mb-2'}>
														Kun Readings
													</div>
													<div className={'flex flex-wrap'}>
														{kanji.listKunReading.length !== 0 ? kanji.listKunReading.map((kunReading, key) => {
															return (
																<KunReading key={key} data={kunReading} />
															);
														}) : <div>-</div>}
													</div>
												</div>
											</div>
											<div className={'mb-2 border-b-2'}>
												<div className={'mb-2'}>
													<div className={'text-lg mb-2'}>
														Name Readings
													</div>
													<div className={'flex flex-wrap'}>
														{kanji.listNameReading.length !== 0 ? kanji.listNameReading.map((nameReading, key) => {
															return (
																<NameReading key={key} data={nameReading} />
															);
														}) : <div>-</div>}
													</div>
												</div>
											</div>
											<div className={'mb-2 border-b-2'}>
												<div className={'mb-2'}>
													<div className={'text-lg mb-2'}>
														Kanji Example
													</div>
												</div>
												{kanji.listMskanjiExample && kanji.listMskanjiExample.length > 0 ? kanji.listMskanjiExample.map((mskanjiExample, key) => {
													return (
														<Example
															key={key}
															kana={mskanjiExample.kana}
															kanji={mskanjiExample.kanjiexample}
															mean={mskanjiExample.mean}
															description={mskanjiExample.description}
														/>
													);
												}) : (
													<div>-</div>
												)}
											</div>
										</div>
									)}
								</div>
							)}
						</div></>
				)}
			</div>
		</>
	);

	if (user === null) {
		return (
			<>
				<Head>
					<title>Master Kanji</title>
				</Head>
				<div className={'p-4 font-sans w-full max-w-4xl mx-auto'}>
					<div className={'mb-4'}>
						<div className={'mb-4 flex justify-between items-center'}>
							<div className={'text-2xl'} >
								Master Kanji
							</div>
						</div>
					</div>
					<div className={'mb-4'}>
						<div>
							Please Login to access this page
						</div>
					</div>
				</div>
			</>
		);
	} else {
		return (
			<>
				<Head>
					<title>Master Kanji</title>
				</Head>
				<div className={'p-4 font-sans w-full max-w-4xl mx-auto'}>
					<div className={'mb-4'}>
						<div className={'mb-4 flex justify-between items-center'}>
							<div className={'text-2xl'} >
								Master Kanji
							</div>
							{!isEmptyObject(kanji) && (
								<Link href={`/mskanji/${kanji.kanji}`} >
									<a>
										<RiPencilLine className={''} size={'1.5em'} />
									</a>
								</Link>
							)}
						</div>
						<input
							type="text"
							value={search}
							onChange={handleChange}
							className={'w-full border-2 rounded h-10 px-2 bg-gray-50 font-notoSerif'}
							placeholder={'Search a kanji'}
						/>
					</div>
					<div className={'mb-4'}>
						{isLoading ? (
							<div className={'w-full h-60 flex justify-center items-center'}>
								<AiOutlineLoading3Quarters className={'animate-spin font-bold'} size={'4em'} />
							</div>
						) : (
							<div>
								{isEmptyObject(kanji) && debounceSearch === '' ? (
									<div>
										<div>To do search</div>
										<div className={'flex'}>
											<div className={'text-blue-600 cursor-pointer flex items-center'} onClick={() => handlePressExample(randomKanji)} >
												<span className={'mr-2'}>{'Example :'}</span>
												<span className={'font-notoSerif'}>{randomKanji}</span>
											</div>
										</div>
									</div>
								) : isEmptyObject(kanji) && debounceSearch !== '' ? (
									<div className={'w-full h-40 flex justify-center items-center'}>
										Data Not Found
									</div>
								) : (
									<div>
										<div className={'flex mb-4'}>
											<div className="flex-shrink-0 h-20 w-20 sm:h-28 sm:w-28">
												<div className={'text-5xl sm:text-6xl font-yujiSyuku select-none h-full w-full flex justify-center items-center bg-gray-50 rounded-lg shadow-md'}>
													{kanji.kanji}
												</div>
											</div>
											<div className="flex-1 h-20 grid grid-cols-3 gap-1 sm:h-28">
												<div className={'flex justify-center items-center flex-col'}>
													<div className={'font-semibold text-lg'}>{kanji.jlpt}</div>
													<div className={'font-light text-sm'}>Jlpt</div>
												</div>
												<div className={'flex justify-center items-center flex-col'}>
													<div className={'font-semibold text-lg'}>{kanji.grade}</div>
													<div className={'font-light text-sm'}>Grade</div>
												</div>
												<div className={'flex justify-center items-center flex-col'}>
													<div className={'font-semibold text-lg'}>{kanji.strokeCount}</div>
													<div className={'font-light text-sm'}>Stroke</div>
												</div>
											</div>
										</div>
										<div className={'mb-2 border-b-2'}>
											<div className={'mb-2'}>
												<div className={'text-lg'}>
													Heisig En
												</div>
												<div className={'capitalize'}>{kanji.heisigEn}</div>
											</div>
										</div>

										<div className={'mb-2 border-b-2'}>
											<div className={'mb-2'}>
												<div className={'text-lg'}>
													Meaning
												</div>
												{kanji.listMeaning.map((meaning, key) => {
													return (
														<div key={key} className={'capitalize'}>
															{(key + 1) + '. ' + meaning}
														</div>
													);
												})}
											</div>
										</div>
										<div className={'mb-2 border-b-2'}>
											<div className={'mb-2'}>
												<div className={'text-lg mb-2'}>
													On Readings
												</div>
												<div className={'flex flex-wrap'}>
													{kanji.listOnReading.length !== 0 ? kanji.listOnReading.map((onReading, key) => {
														return (
															<OnReading key={key} data={onReading} />
														);
													}) : <div>-</div>}
												</div>
											</div>
										</div>
										<div className={'mb-2 border-b-2'}>
											<div className={'mb-2'}>
												<div className={'text-lg mb-2'}>
													Kun Readings
												</div>
												<div className={'flex flex-wrap'}>
													{kanji.listKunReading.length !== 0 ? kanji.listKunReading.map((kunReading, key) => {
														return (
															<KunReading key={key} data={kunReading} />
														);
													}) : <div>-</div>}
												</div>
											</div>
										</div>
										<div className={'mb-2 border-b-2'}>
											<div className={'mb-2'}>
												<div className={'text-lg mb-2'}>
													Name Readings
												</div>
												<div className={'flex flex-wrap'}>
													{kanji.listNameReading.length !== 0 ? kanji.listNameReading.map((nameReading, key) => {
														return (
															<NameReading key={key} data={nameReading} />
														);
													}) : <div>-</div>}
												</div>
											</div>
										</div>
										<div className={'mb-2 border-b-2'}>
											<div className={'mb-2'}>
												<div className={'text-lg mb-2'}>
													Kanji Example
												</div>
											</div>
											{kanji.listMskanjiExample && kanji.listMskanjiExample.length > 0 ? kanji.listMskanjiExample.map((mskanjiExample, key) => {
												return (
													<Example
														key={key}
														kana={mskanjiExample.kana}
														kanji={mskanjiExample.kanjiexample}
														mean={mskanjiExample.mean}
														description={mskanjiExample.description}
													/>
												);
											}) : (
												<div>-</div>
											)}
										</div>

									</div>
								)}
							</div>
						)}
					</div>
				</div>
			</>
		);
	}
};

(Mskanji as PageWithLayoutType).layout = Main;

export const getServerSideProps: GetServerSideProps = async (context) => {
	const kanji = context.query.kanji ? context.query.kanji : '';

	return {
		props: {
			qKanji: kanji
		}
	};
};

export default Mskanji;