import { useContext } from 'react';
import Link from 'next/link';
import NotifContext from '@stores/notifContext';
import * as Yup from 'yup';
import { Form, Formik, FormikValues } from 'formik';
import { useMutation } from 'react-query';
import TextField from '@com/Formik/TextField';
import ButtonSubmit from '@com/Formik/ButtonSubmit';
import { Api } from '@lib/Api';
import Router from 'next/router';
import PageWithLayoutType from '@type/layout';
import Main from '@com/Layout/Main';
import UserContext from '@stores/userProvider';

interface Props {

}

let schema = Yup.object().shape({
	username: Yup.string().required(),
	passwd: Yup.string().required(),
});

const Signin: React.FC<Props> = ({ }) => {
	const { notif } = useContext(NotifContext);
	const { user, setUser } = useContext(UserContext);

	const initFormikValue = {
		username: '',
		passwd: '',
	};

	const { data, mutate, isLoading } = useMutation((val: FormikValues) => Api.post('/sign-in', val));

	const handleSubmit = (values: FormikValues, setErrors) => {
		mutate(values, {
			onSuccess: (res) => {
				if (res) {
					if (res.success) {
						setUser(res.payload);
						Router.push('/');
					} else if (res.error) {
						if (res.payload && res.payload.listError) {
							setErrors(res.payload.listError);
						} else {
							notif.error(res.message);
						}
					}
				}
			},
			onError: (res) => {
				notif.error('Please cek you connection');
			}
		});
	};

	return (
		<div className={'h-screen w-screen bg-gray-200 flex justify-center items-center -mt-16'}>
			<div className={'px-4 w-full max-w-md'}>
				<div className={'w-full bg-white rounded-lg shadow p-4 mb-2'}>
					<Formik
						initialValues={initFormikValue}
						validationSchema={schema}
						enableReinitialize={true}
						onSubmit={(values, { setErrors }) => handleSubmit(values, setErrors)}
					>
						{() => {
							return (
								<Form>
									<div className={'flex justify-center'}>
										<span className={'text-xl'}>Sign In</span>
									</div>
									<div className={''}>
										<div className="mb-4">
											<TextField
												label={'Username'}
												name={'username'}
												type={'text'}
												placeholder={'Username'}
											/>
										</div>
										<div className="mb-4">
											<TextField
												label={'Password'}
												type={'password'}
												name={'passwd'}
												placeholder={'Password'}
											/>
										</div>
										<div className={''}>
											<ButtonSubmit
												label={'Sign In'}
												disabled={isLoading}
												loading={isLoading}
											/>
										</div>
									</div>
								</Form>
							);
						}}
					</Formik>
				</div>
				<div className={'flex'}>
					<div className={'mr-1'}>
						{'Don\'t have an account yet?'}
					</div>
					<Link href={'/sign-up'} passHref>
						<a className={'text-blue-600'}>
							<div>Register Now</div>
						</a>
					</Link>
				</div>
			</div>
		</div>
	);
};

(Signin as PageWithLayoutType).layout = Main;

export default Signin;