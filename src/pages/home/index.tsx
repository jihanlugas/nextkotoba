import Main from '@com/Layout/Main';
import PageWithLayoutType from '@type/layout';
import Head from 'next/head';
import React from 'react';

interface Props {

}

const Home: React.FC<Props> = ({ }) => {
	return (
		<>
			<Head>
				<title>{'Home'}</title>
				<meta name="og:title" content="Home | Belajar Bahasa Jepang Online | kotoba" />
				<meta name="og:description" content="..." />
			</Head>
			<div className={'p-4 font-sans w-full max-w-4xl mx-auto'}>
				<div className={'mb-4'}>
					<div className={'flex justify-between items-center'}>
						<h1 className={''} >
							Home
						</h1>
					</div>
				</div>
				<div className={'mb-4'}>
					<h2>Home content</h2>
				</div>
			</div>
		</>
	);
};

(Home as PageWithLayoutType).layout = Main;

export default Home;