import { useContext } from 'react';
import Head from 'next/head';
import Image from 'next/image';
import Link from 'next/link';
import NotifContext from '@stores/notifContext';
import * as Yup from 'yup';
import { Form, Formik, FormikValues } from 'formik';
import { useMutation } from 'react-query';
import TextField from '@com/Formik/TextField';
import ButtonSubmit from '@com/Formik/ButtonSubmit';
import type { NextPage } from 'next';
import { Api } from '@lib/Api';
import Router from 'next/router';
import PageWithLayoutType from '@type/layout';
import Main from '@com/Layout/Main';

interface Props {

}

let schema = Yup.object().shape({
	email: Yup.string().email().required(),
	fullname: Yup.string().required(),
	noHp: Yup.number().required(),
	username: Yup.string().trim().required(),
	passwd: Yup.string().required(),
	confirmPasswd: Yup.string().required().oneOf([Yup.ref('passwd'), null], 'Passwords must match')
});

const Signup: React.FC<Props> = ({ }) => {
	const { notif } = useContext(NotifContext);

	const initFormikValue = {
		email: '',
		fullname: '',
		noHp: '',
		username: '',
		passwd: '',
		confirmPasswd: '',
	};

	const { data, mutate, isLoading } = useMutation((val: FormikValues) => Api.post('/sign-up', val));

	const handleSubmit = (values: FormikValues, setErrors) => {
		mutate(values, {
			onSuccess: (res) => {
				if (res) {
					if (res.success) {
						notif.success(res.message);
					} else if (res.error) {
						if (res.payload && res.payload.listError) {
							setErrors(res.payload.listError);
						} else {
							notif.error(res.message);
						}
					}
				}
			},
			onError: (res) => {
				notif.error('Please cek you connection');
			}
		});
	};

	return (
		<div className={'h-screen w-screen bg-gray-200 flex justify-center items-center -mt-16'}>
			<div className={'px-4 w-full max-w-md'}>
				<div className={'w-full bg-white rounded-lg shadow p-4 mb-2'}>
					<Formik
						initialValues={initFormikValue}
						validationSchema={schema}
						enableReinitialize={true}
						onSubmit={(values, { setErrors }) => handleSubmit(values, setErrors)}
					>
						{() => {
							return (
								<Form>
									<div className={'flex justify-center'}>
										<span className={'text-xl'}>Sign Up</span>
									</div>
									<div className={''}>
										<div className="mb-4">
											<TextField
												label={'Username'}
												name={'username'}
												type={'text'}
												placeholder={'Username'}
											/>
										</div>
										<div className="mb-4">
											<TextField
												label={'Email'}
												name={'email'}
												type={'text'}
												placeholder={'Email'}
											/>
										</div>
										<div className="mb-4">
											<TextField
												label={'No Hp'}
												name={'noHp'}
												type={'text'}
												placeholder={'No Hp'}
											/>
										</div>
										<div className="mb-4">
											<TextField
												label={'Fullname'}
												name={'fullname'}
												type={'text'}
												placeholder={'Fullname'}
											/>
										</div>
										<div className="mb-4">
											<TextField
												label={'Password'}
												type={'password'}
												name={'passwd'}
												placeholder={'Password'}
											/>
										</div>
										<div className="mb-4">
											<TextField
												label={'Confirm Password'}
												type={'password'}
												name={'confirmPasswd'}
												placeholder={'Confirm Password'}
											/>
										</div>
										<div className={''}>
											<ButtonSubmit
												label={'Sign In'}
												disabled={isLoading}
												loading={isLoading}
											/>
										</div>
									</div>
								</Form>
							);
						}}
					</Formik>
				</div>
				<div className={'flex'}>
					<div className={'mr-1'}>
						{'Already have an account ?'}
					</div>
					<Link href={'/sign-in'} passHref>
						<a className={'text-blue-600'}>
							<div>Sign In</div>
						</a>
					</Link>
				</div>
			</div>
		</div>
	);
};

(Signup as PageWithLayoutType).layout = Main;

export default Signup;