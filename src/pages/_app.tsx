import React from 'react';
import Head from 'next/head';
import '../styles/globals.css';
import type { AppProps, AppContext } from 'next/app';
import { QueryClient, QueryClientProvider } from 'react-query';
import { ReactQueryDevtools } from 'react-query/devtools';
import { NotifContextProvider } from '@stores/notifContext';
import { UserContextProvider } from '@stores/userProvider';
import App from 'next/app';
import PageWithLayoutType from '@type/layout';
import { GetServerSideProps } from 'next';


function isBrowser() {
	return typeof window !== 'undefined';
}

type AppLayoutProps = {
	Component: PageWithLayoutType
	pageProps: any
}

// function MyApp({ Component, pageProps }: AppLayoutProps) {
const MyApp: React.FC<AppLayoutProps> = ({ Component, pageProps }) => {

	const queryClient = new QueryClient();

	const Layout = Component.layout || (({ children }) => <>{children}</>);

	return (
		<React.Fragment>
			<Head>
				<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
				<meta name="description" content="Belajar Bahasa Jepang Online" />

				<meta name="og:title" content="Belajar Bahasa Jepang Online | kotoba" />
				<meta name="og:description" content="Belajar Bahasa Jepang Online" />
				<meta name="og:image" content={process.env.APP_ICON_512} />
				<meta name="google-site-verification" content="d_Z0doP666Lm0u8e3DB4BMNCrtutRdIkup20TTWPEVM" />
				<title>{process.env.APP_NAME}</title>
			</Head>
			<NotifContextProvider>
				<UserContextProvider>
					<QueryClientProvider client={queryClient}>
						<Layout>
							<Component {...pageProps} />
						</Layout>
						{/* <ReactQueryDevtools /> */}
					</QueryClientProvider>
				</UserContextProvider>
			</NotifContextProvider>
		</React.Fragment>
	);
};

// export const getServerSideProps: GetServerSideProps = async (context) => {
// 	// console.log('context ', context);
// 	// const { kanji } = context.query;

// 	// const data = await Api.get('/kanji/' + kanji).then(res => res);

// 	// console.log('context ', context.req.headers.cookie);
// 	// console.log('kanji ', kanji);
// 	// console.log('data ', data);

// 	return {
// 		props: {
// 			// qKanji: kanji
// 		}
// 	};
// };

// MyApp.getInitialProps = async (ctx: AppContext) => {
// 	console.log('ctx ', ctx.cookies)
// 	console.log('ctx ', ctx.ctx.cookies)
// 	const { pathname, req, res } = ctx.ctx;
// 	console.log('req ', req.cookies)

// 	const appProps = await App.getInitialProps(ctx);

// 	return { ...appProps };
// };

export default MyApp;
