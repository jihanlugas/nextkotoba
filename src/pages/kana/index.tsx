import Main from '@com/Layout/Main';
import PageWithLayoutType from '@type/layout';
import Head from 'next/head';
import React from 'react';
import Kana from '@com/Card/kana';



interface Props {
}


const Index: React.FC<Props> = () => {
	return (
		<>
			<Head>
				<title>{'Kana'}</title>
				<meta name="og:title" content={'Kana | Hiragana | Katakana | Belajar Bahasa Jepang Online | kotoba'} />
				<meta name="og:description" content="..." />
			</Head>
			<div className={'p-4 font-sans w-full max-w-4xl mx-auto'}>
				<div className={'mb-4'}>
					<div className={'mb-4 flex justify-between items-center'}>
						<h1 className={''} >
							Kana
						</h1>
					</div>
				</div>
				<div className={'mb-4 '}>
					<Kana />
				</div>
			</div>
		</>
	);
};

(Index as PageWithLayoutType).layout = Main;

export default Index;