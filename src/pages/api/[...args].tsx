import { createProxyMiddleware } from 'http-proxy-middleware';
import https from 'https';

export default createProxyMiddleware('/api', {
	target: process.env.API_END_POINT, //the data server
	changeOrigin: true,
	agent: new https.Agent({
		rejectUnauthorized: false,
	}),
	pathRewrite: {
		'^/api': ''
	}
});

export const config = {
	api: {
		bodyParser: false
	}
};