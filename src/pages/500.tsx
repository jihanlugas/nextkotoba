import React from 'react';
import PageWithLayoutType from '@type/layout';
import Main from '@com/Layout/Main';

interface Props {
	errorCode?: number;
	title?: string;
}

const Custom500: React.FC<Props> = ({ errorCode, title }) => {
	return (
		<div className={'fixed h-screen w-screen flex flex-col justify-center items-center'}>
			<div className='text-6xl mb-4'>500</div>
			<div className='text-lg'>
				{title ? title : 'Something went wrong'}
			</div>
		</div>
	);
};

(Custom500 as PageWithLayoutType).layout = Main;

export default Custom500;

