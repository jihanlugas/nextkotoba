import Main from '@com/Layout/Main';
import { Api } from '@lib/Api';
import Head from 'next/head';
import React, { useContext, useEffect, useState } from 'react';
import { useQuery } from 'react-query';
import { AiOutlineLoading3Quarters } from 'react-icons/ai';
import { useRouter } from 'next/router';
import PageWithLayoutType from '@type/layout';
import UserContext from '@stores/userProvider';


interface IkanjiExample {
	mskanjiexampleId?: number,
	mskanjiId?: number,
	kanjiexample?: string,
	kana?: string,
	mean?: string,
	description?: string
}


interface Ikanji {
	mskanjiId?: number,
	kanji?: string,
	grade?: string,
	jlpt?: string,
	unicode?: string,
	heisigEn?: string,
	strokeCount?: string,
	listMeaning?: string[],
	listKunReading?: string[],
	listOnReading?: string[],
	listNameReading?: string[],
	listMskanjiExample?: IkanjiExample[],
}

interface Props {
}


const Index: React.FC<Props> = () => {
	const { isLoading, error, data, isFetching, refetch } = useQuery('kanji', () => Api.get('/kanji/list'));
	const { user, setUser } = useContext(UserContext);

	const [kanjis, setKanjis] = useState<Ikanji[]>([]);
	const router = useRouter();

	useEffect(() => {
		if (data && data.success) {
			setKanjis(data.payload);
		}
	}, [data]);

	const handleClickKanji = (kanji: string) => {
		router.push('/kanji/' + kanji);
	};

	useEffect(() => {
		if (user) {
			refetch();
		}
	}, [user]);

	// console.log('user ', user);

	return (
		<>
			<Head>
				<title>{'Kanji Kanji'}</title>
				<meta name="og:title" content="Kanji | Kanji | Belajar Bahasa Jepang Online | kotoba" />
				<meta name="og:description" content="..." />
			</Head>
			<div className={'p-4 font-sans w-full max-w-4xl mx-auto'}>
				<div className={'mb-4'}>
					<div className={'mb-4 flex justify-between items-center'}>
						<h1 className={''} >
							{'Kanji'}
						</h1>
					</div>
				</div>
				{user === null ? (
					<div className={'mb-4'}>
						<div>
							Please Login to access this page
						</div>
					</div>
				) : !user.userId ? (
					<div className={'mb-4'}>
					</div>
				) : (
					<div className={'mb-4'}>
						{isLoading ? (
							<div className={'w-full h-60 flex justify-center items-center'}>
								<AiOutlineLoading3Quarters className={'animate-spin font-bold'} size={'4em'} />
							</div>
						) : (
							<div>
								{kanjis.length > 0 ? (
									<div className="grid gap-4 grid-cols-3 sm:grid-cols-4 md:grid-cols-5">
										{kanjis.map((kanji, key) => {
											return (
												<div key={key} className={'bg-white shadow-md rounded-md h-20 flex justify-center items-center animate-fadeIn hover:bg-gray-200'} onClick={() => handleClickKanji(kanji.kanji)}>
													<div className={'font-yujiSyuku select-none'}>
														<h2 className={'text-4xl'}>{kanji.kanji}</h2>
													</div>
												</div>
											);
										})}
									</div>
								) : (
									<div>
										<div>Tidak ada data</div>
									</div>
								)}
							</div>
						)}
					</div>
				)}
			</div>
		</>
	);
};

(Index as PageWithLayoutType).layout = Main;

export default Index;