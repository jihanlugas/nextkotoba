import Head from 'next/head';
import Main from '@com/Layout/Main';
import { BiChevronRight } from 'react-icons/bi';
import { RiPencilLine } from 'react-icons/ri';
import { AiOutlineLoading3Quarters } from 'react-icons/ai';
import Link from 'next/link';
import { useEffect, useState, Fragment, useContext } from 'react';
import { Api } from '../../lib/Api';
import { useQuery, useMutation } from 'react-query';
import { isEmptyObject } from '@utils/Validate';
import NotifContext from '@stores/notifContext';
import { GetServerSideProps } from 'next';
import PageWithLayoutType from '@type/layout';
import Example from '@com/View/Example';
import OnReading from '@com/View/OnReading';
import KunReading from '@com/View/KunReading';
import NameReading from '@com/View/NameReading';
import Custom404 from '@pages/404';


interface Props {
	qKanji: string
}

interface IkanjiExample {
	kanjiexampleId?: number,
	mskanjiId?: number,
	kanjiId?: number,
	userId?: number,
	kanjiexample?: string,
	kana?: string,
	mean?: string,
	description?: string
}

interface Ikanji {
	kanjiId?: number,
	mskanjiId?: number,
	userId?: number,
	kanji?: string,
	grade?: string,
	jlpt?: string,
	unicode?: string,
	heisigEn?: string,
	strokeCount?: string,
	listMeaning?: string[],
	listKunReading?: string[],
	listOnReading?: string[],
	listNameReading?: string[],
	listKanjiExample?: IkanjiExample[],
}

const Kanji: React.FC<Props> = (props) => {
	const { qKanji } = props;
	const [kanji, setKanji] = useState<Ikanji>({});

	const { isLoading, error, data, isFetching, refetch } = useQuery('kanji', () => Api.get('/kanji/' + qKanji));

	const { notif } = useContext(NotifContext);

	useEffect(() => {
		if (data && data.success) {
			setKanji(data.payload);
		} else {
			if ((data && data.error)) {
				// notif.error(data.message)
			}
			setKanji({});
		}
	}, [data]);

	if (isLoading || isEmptyObject(kanji)) {
		return (
			<>
				<Head>
					<title>{'Kanji - ' + qKanji}</title>
				</Head>
				<div className={'p-4 font-sans w-full max-w-4xl mx-auto'}>
					{!isLoading && isEmptyObject(kanji) ? (
						<Custom404 title={'Anda belum menyimpan ' + qKanji + ' Simpan dari master kanji'} />
					) : (
						<div className={'w-full h-60 flex justify-center items-center'}>
							<AiOutlineLoading3Quarters className={'animate-spin font-bold'} size={'4em'} />
						</div>
					)}
				</div>
			</>
		);
	}


	return (
		<>
			<Head>
				<title>{'Kanji - ' + qKanji}</title>
				<meta name="og:title" content={'Kanji | Kanji - ' + qKanji + '| Belajar Bahasa Jepang Online | kotoba'} />
				<meta name="og:description" content="..." />
			</Head>
			<div className={'p-4 font-sans w-full max-w-4xl mx-auto'}>
				<div className={'mb-4 flex items-center justify-between'}>
					<div className={'text-2xl flex items-center'}>
						<Link href={{ pathname: '/kanji' }}>
							<a>
								<h1 className={'mr-2'} >
									{'Kanji'}
								</h1>
							</a>
						</Link>
						<BiChevronRight className={'mr-2'} size={'1em'} />
						<div className={'mr-2 font-notoSerif'} >{qKanji}</div>
					</div>
					{!isEmptyObject(kanji) && (
						<Link href={`/kanji/${kanji.kanji}/edit`} >
							<a>
								<RiPencilLine className={''} size={'1.5em'} />
							</a>
						</Link>
					)}
				</div>
				<div>
					<div className={'flex mb-4'}>
						<div className="shrink-0 h-20 w-20 sm:h-28 sm:w-28">
							<div className={'text-5xl sm:text-6xl font-yujiSyuku select-none h-full w-full flex justify-center items-center bg-gray-50 rounded-lg shadow-md animate-fadeIn'}>
								{kanji.kanji}
							</div>
						</div>
						<div className="flex-1 h-20 grid grid-cols-3 gap-1 sm:h-28">
							<div className={'flex justify-center items-center flex-col'}>
								<div className={'font-semibold text-lg'}>{kanji.jlpt}</div>
								<div className={'font-light text-sm'}>Jlpt</div>
							</div>
							<div className={'flex justify-center items-center flex-col'}>
								<div className={'font-semibold text-lg'}>{kanji.grade}</div>
								<div className={'font-light text-sm'}>Grade</div>
							</div>
							<div className={'flex justify-center items-center flex-col'}>
								<div className={'font-semibold text-lg'}>{kanji.strokeCount}</div>
								<div className={'font-light text-sm'}>Stroke</div>
							</div>
						</div>
					</div>
					<div className={'mb-2 border-b-2'}>
						<div className={'mb-2'}>
							<div className={'text-lg'}>
								Heisig En
							</div>
							<div className={'capitalize'}>{kanji.heisigEn}</div>
						</div>
					</div>
					<div className={'mb-2 border-b-2'}>
						<div className={'mb-2'}>
							<div className={'text-lg'}>
								Meaning
							</div>
							{kanji.listMeaning.map((meaning, key) => {
								return (
									<div key={key} className={'capitalize'}>
										{(key + 1) + '. ' + meaning}
									</div>
								);
							})}
						</div>
					</div>
					<div className={'mb-2 border-b-2'}>
						<div className={'mb-2'}>
							<div className={'text-lg mb-2'}>
								On Readings
							</div>
							<div className={'flex flex-wrap'}>
								{kanji.listOnReading.length !== 0 ? kanji.listOnReading.map((onReading, key) => {
									return (
										<OnReading key={key} data={onReading} />
									);
								}) : <div>-</div>}
							</div>
						</div>
					</div>
					<div className={'mb-2 border-b-2'}>
						<div className={'mb-2'}>
							<div className={'text-lg mb-2'}>
								Kun Readings
							</div>
							<div className={'flex flex-wrap'}>
								{kanji.listKunReading.length !== 0 ? kanji.listKunReading.map((kunReading, key) => {
									return (
										<KunReading key={key} data={kunReading} />
									);
								}) : <div>-</div>}
							</div>
						</div>
					</div>
					<div className={'mb-2 border-b-2'}>
						<div className={'mb-2'}>
							<div className={'text-lg mb-2'}>
								Name Readings
							</div>
							<div className={'flex flex-wrap'}>
								{kanji.listNameReading.length !== 0 ? kanji.listNameReading.map((nameReading, key) => {
									return (
										<NameReading key={key} data={nameReading} />
									);
								}) : <div>-</div>}
							</div>
						</div>
					</div>
					<div className={'mb-2 border-b-2'}>
						<div className={'mb-2'}>
							<div className={'text-lg mb-2'}>
								Kanji Example
							</div>
						</div>
						{kanji.listKanjiExample && kanji.listKanjiExample.length > 0 ? kanji.listKanjiExample.map((kanjiExample, key) => {
							return (
								<Example
									key={key}
									kana={kanjiExample.kana}
									kanji={kanjiExample.kanjiexample}
									mean={kanjiExample.mean}
									description={kanjiExample.description}
								/>
							);
						}) : (
							<div>-</div>
						)}
					</div>
				</div>
			</div>
		</>
	);
};

(Kanji as PageWithLayoutType).layout = Main;

export const getServerSideProps: GetServerSideProps = async (context) => {
	const { kanji } = context.query;
	console.log('kanji ', context);

	// const data = await Api.get('/kanji/' + kanji).then(res => res);

	// console.log('context ', context.req.headers.cookie);
	// console.log('kanji ', kanji);
	// console.log('data ', data);

	return {
		props: {
			qKanji: kanji
		}
	};
};


export default Kanji;