import Head from 'next/head';
import Main from '@com/Layout/Main';
import Link from 'next/link';
import PageWithLayoutType from '@type/layout';


interface Props {
}

const Index: React.FC<Props> = () => {
	return (
		<>
			<Head>
				<title>{'Materi'}</title>
				<meta name="og:title" content={'Materi | Belajar Bahasa Jepang Online | kotoba'} />
				<meta name="og:description" content="..." />
			</Head>
			<div className={'p-4 font-sans w-full max-w-4xl mx-auto'}>
				<div className={'mb-4 flex text-2xl items-center'}>
					<div className={'mr-2'} >{'Materi'}</div>
				</div>
				<div className={'p-4 shadow rounded-md bg-white font-notoSerif'}>
					<div className={'mb-4'}>
						<Link href={{ pathname: '/materi/tata-bahasa' }}>
							<a className='text-green-400 cursor-pointer'>
								<h2 className={'text-lg mr-2'}>
									{'Tata Bahasa'}
								</h2>
							</a>
						</Link>
					</div>
					<div className={'mb-4'}>
						<Link href={{ pathname: '/materi/kosakata' }}>
							<a className='text-green-400 cursor-pointer'>
								<h2 className={'text-lg mr-2'}>
									{'Kosakata'}
								</h2>
							</a>
						</Link>
					</div>
				</div>
			</div>
		</>
	);
};


(Index as PageWithLayoutType).layout = Main;

export default Index;