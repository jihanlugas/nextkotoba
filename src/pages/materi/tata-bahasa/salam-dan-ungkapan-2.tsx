import React from 'react';
import Head from 'next/head';
import Main from '@com/Layout/Main';
import Link from 'next/link';
import PageWithLayoutType from '@type/layout';
import { BiChevronRight, BiChevronLeft } from 'react-icons/bi';


interface Props {
}

const content = [
	{
		title: 'Saat memperkenalkan diri 1',
		subcontent: [
			{
				title: '',
				data: [
					{
						data: [
							{
								japan: 'はじめまして。',
								romaji: 'Hajime mashite',
								indonesia: 'Salam kenal.',
							},
							{
								japan: '( nama ) です。',
								romaji: '( nama ) desu.',
								indonesia: 'Nama saya ( nama ).',
							},
							{
								japan: 'どうぞよろしくおねがいします。',
								romaji: 'Douzo yoroshiku onegai shi-masu.',
								indonesia: 'Mohon bimbingannya.',
							},
						],
						deskripsi: '"Hajime mashite" diucapkan saat mengawali perkenal pertama, sedangkan "douzo yoroshiku onegai shi-masu" dapat digunakan saat mengahiri perkenalan.\n\nCara memperkenalkan nama diri sangat sederhana, yaitu "(nama) + desu" saja. Fungsi "desu" seperti verb "be (is, am, are, dll)" dalam bahasa Inggris, dan membentuk predikat secara sopan.',
					},
				],
			}
		]
	},
	{
		title: 'Saat minta tolong',
		subcontent: [
			{
				title: '',
				data: [
					{
						data: [
							{
								japan: 'おねがいします。',
								romaji: 'Onegai shi-masu.',
								indonesia: 'Minta tolong',
							},
						],
						deskripsi: 'Ungkapan ini digunakan saat minta bantuan, pertolongan, persetujuan, dan seterusnya dari lawan bicara.',
					},
				],
			}
		]
	},
	{
		title: 'Saat berangkat dan kembali',
		subcontent: [
			{
				title: 'Di rumah',
				data: [
					{
						data: [
							{
								japan: 'いってきます。',
								romaji: 'Itte ki-masu.',
								indonesia: 'Saya berangkat ya./ Pergi dulu ya.',
							},
							{
								japan: 'いってらっしゃい。',
								romaji: 'Itte rasshai.',
								indonesia: 'Selamat jalan.',
							},
						],
						deskripsi: '"Itte ki-masu" diucapkan oleh orang yang akan berangkat dari rumah kepada anggota yang ditinggal di rumah. Sedangkan, "Itte rasshai" diucapkan oleh orang yang ditinggal di rumah kepada anggota yang akan berangkat dari rumah.',
					},
					{
						data: [
							{
								japan: 'ただいま。',
								romaji: 'Tadaima.',
								indonesia: 'Saya pulang',
							},
							{
								japan: 'おかえりなさい。',
								romaji: 'Okaeri nasai.',
								indonesia: 'Selamat datang',
							},
						],
						deskripsi: 'Saat pulang ke rumah, orang yang pulang ke rumah mengucapkan "Tadaima" kepada anggota yang ada di rumah, sedangkan "Okaeri nasai" diucapkan oleh orang yang ada dirumah kepada anggota yang baru pulang ke rumah.',
					},
				],
			},
			{
				title: 'Di kantor',
				data: [
					{
						data: [
							{
								japan: 'いってきます。/ いってまいります。',
								romaji: 'Itte ki-masu. / Itte mairi masu',
								indonesia: 'Saya berangkat',
							},
							{
								japan: 'いってらっしゃい（ませ）。',
								romaji: 'Itte rasshai-(mase).',
								indonesia: 'Selamat jalan./ Hati-hati, sampai kembali lagi.',
							},
							{
								japan: 'ただいまもどりました。',
								romaji: 'Tadaima modori-mashita.',
								indonesia: 'Saya pulang.',
							},
							{
								japan: 'おかえりなさい（ませ）。',
								romaji: 'Okaeri nasai-(mase).',
								indonesia: 'Saya pulang.',
							},
						],
						deskripsi: 'Penggunaannya sama dengan "Itte ki-masu / Itte rasshai" dan "Tadaima / Okaeri" yang telah dijelaskan di atas. Hanya saja, pada umumnya, percakapan-percakapan yang digunakan di kantor atau dalam kegiatan bisnis sering lebih sopan daripada percakapan biasa dalam kehidupan sehari-hari. "Itte mairi-masu (saya berangkat)", "Itte rasshai-mase (selamat pulang)" lebih sopan daripada "Itte ki-masu (saya berangkat)", "Itte rasshai (selamat jalan)", "Tadaima (saya pulang)", dan "Okaeri nasai (selamat pulang)".',
					},
				],
			},
		]
	},
	{
		title: 'Saat menanyakan dan menyatakan keadaan',
		subcontent: [
			{
				title: '',
				data: [
					{
						data: [
							{
								japan: 'だいじょうぶですか。',
								romaji: 'Daijoubu desu ka.',
								indonesia: 'Tidak apa-apa? / Tidak masalah?',
							},
							{
								japan: 'だいじょうぶです。',
								romaji: 'Daijoubu desu.',
								indonesia: 'Tidak apa-apa. / Tidak masalah.',
							},
						],
						deskripsi: '"Daijoubu desu" digunakan untuk menyatakan keadaan yang bebas dari bahaya atau kekhawatiran. Sedangkan, "Daijoubu desu ka" merupakan kalimat tanya dari "Daijoubu desu". "Ka" yang dibubuhkan pada akhir kalimat menunjukan bahwa kalimat tersebut merupakan kalimat tanya.',
					},
				],
			}
		]
	},
];


const SalamDanUngkapan2: React.FC<Props> = () => {

	const [tap, setTap] = React.useState('PENJELASAN');

	return (
		<>
			<Head>
				<title>{'Tata Bahasa - Salam dan Ungkapan 2'}</title>
				<meta name="og:title" content={'Materi | Tata Bahasa | Salam dan Ungkapan | Salam dan Ungkapan 2 | Belajar Bahasa Jepang Online | kotoba'} />
				<meta name="og:description" content="..." />
			</Head>
			<div className={'p-4 font-sans w-full max-w-4xl mx-auto'}>
				<div className={'w-full text-2xl mb-4'}>
					<div className={'hidden sm:flex  items-center'}>
						<Link href={{ pathname: '/materi' }}>
							<a>
								<h1 className={'mr-2'} >{'Materi'}</h1>
							</a>
						</Link>
						<BiChevronRight className={'mr-2'} size={'1em'} />
						<Link href={{ pathname: '/materi/tata-bahasa' }}>
							<a>
								<h1 className={'mr-2'} >{'Tata Bahasa'}</h1>
							</a>
						</Link>
						<BiChevronRight className={'mr-2'} size={'1em'} />
						<h1 className={'mr-2'} >{'Salam dan Ungkapan 2'}</h1>
					</div>
					<div className={'flex sm:hidden items-center'}>
						<Link href={{ pathname: '/materi/tata-bahasa' }}>
							<a>
								<BiChevronLeft className={'mr-2'} size={'1em'} />
							</a>
						</Link>
						<h1 className={'mr-2'} >{'Salam dan Ungkapan 2'}</h1>
					</div>
				</div>
				<div className={'p-4 shadow rounded-md bg-white font-notoSerif'}>
					<div className={''}>
						<div className={'flex mb-8'}>
							<div className={tap === 'PENJELASAN' ? 'p-2 bg-white rounded-t-md border-b-2 border-green-400 font-semibold' : 'p-2'} onClick={() => setTap('PENJELASAN')}>
								Penjelasan
							</div>
						</div>
						<div className={tap === 'PENJELASAN' ? 'block' : 'hidden'}>
							<Penjelasan />
						</div>
					</div>
				</div>
			</div>
		</>
	);
};

const Penjelasan = () => {
	return (
		<>
			{content.map((v, key) => {
				return (
					<div className={'bg-white '} key={key}>
						<h2 className={'border-l-8 border-green-400 py-2 pl-4 mb-4'}>
							{v.title}
						</h2>
						<div>
							{v.subcontent.map((v, i) => {
								return (
									<div className={'mb-8'} key={i}>
										{v.title && (
											<h3 className={'mb-8 border-b-2'}>
												{v.title}
											</h3>
										)}
										<div className={'mb-4'}>
											{v.data.map((v, i) => {
												return (
													<div className={'mb-4'} key={i}>
														<div className={'mb-4'}>
															{v.data.map((v, i) => {
																return (
																	<div className={'mb-4'} key={i}>
																		<div className={'text-xl flex'}>
																			<div className={'bg-yellow-100 mb-2 px-2 font-semibold'}>
																				{v.japan}
																			</div>
																		</div>
																		<div className={'text-xl flex'}>
																			<div className={'bg-green-100 mb-2 px-2 font-semibold'}>
																				{v.romaji}
																			</div>
																		</div>
																		<div className={'text-xl flex'}>
																			<div className={'bg-red-100 mb-2 px-2 font-semibold'}>
																				{v.indonesia}
																			</div>
																		</div>
																	</div>
																);
															})}
														</div>
														<div className={'mb-4'}>
															<p className={'text-justify mb-4 whitespace-pre-wrap'}>
																{v.deskripsi}
															</p>
														</div>
													</div>
												);
											})}
										</div>
									</div>
								);
							})}
						</div>
					</div>
				);
			})}
		</>
	);
};


(SalamDanUngkapan2 as PageWithLayoutType).layout = Main;

export default SalamDanUngkapan2;