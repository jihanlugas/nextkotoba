import React from 'react';
import Head from 'next/head';
import Main from '@com/Layout/Main';
import Link from 'next/link';
import PageWithLayoutType from '@type/layout';
import { BiChevronRight, BiChevronLeft } from 'react-icons/bi';


interface Props {
}


const PerbedaanIndonesiaJepang: React.FC<Props> = () => {

	const [tap, setTap] = React.useState('PENJELASAN');

	return (
		<>
			<Head>
				<title>{'Tata Bahasa - Perbedaan Bahasa Indonesia dengan Jepang'}</title>
				<meta name="og:title" content={'Materi | Tata Bahasa | Perbedaan Bahasa Indonesia dengan Jepang | Belajar Bahasa Jepang Online | kotoba'} />
				<meta name="og:description" content="..." />
			</Head>
			<div className={'p-4 font-sans w-full max-w-4xl mx-auto'}>
				<div className={'w-full text-2xl mb-4'}>
					<div className={'hidden sm:flex  items-center'}>
						<Link href={{ pathname: '/materi' }}>
							<a>
								<div className={'mr-2'} >{'Materi'}</div>
							</a>
						</Link>
						<BiChevronRight className={'mr-2'} size={'1em'} />
						<Link href={{ pathname: '/materi/tata-bahasa' }}>
							<a>
								<div className={'mr-2'} >{'Tata Bahasa'}</div>
							</a>
						</Link>
						<BiChevronRight className={'mr-2'} size={'1em'} />
						<div className={'mr-2'} >{'Perbedaan Bahasa'}</div>
					</div>
					<div className={'flex sm:hidden items-center'}>
						<Link href={{ pathname: '/materi/tata-bahasa' }}>
							<a>
								<BiChevronLeft className={'mr-2'} size={'1em'} />
							</a>
						</Link>
						<div className={'mr-2'} >{'Perbedaan Bahasa'}</div>
					</div>
				</div>
				<div className={'p-4 shadow rounded-md bg-white font-notoSerif'}>
					<div className={''}>
						<div className={'flex mb-8'}>
							<div className={tap === 'PENJELASAN' ? 'p-2 bg-white rounded-t-md border-b-2 border-green-400 font-semibold' : 'p-2'} onClick={() => setTap('PENJELASAN')}>
								Penjelasan
							</div>
						</div>
						<div className={tap === 'PENJELASAN' ? 'block' : 'hidden'}>
							<Penjelasan />
						</div>
					</div>
				</div>
			</div>
		</>
	);
};

const Title = ({ text }: { text: string }) => {
	return (
		<h2 className={'border-l-8 border-green-400 py-2 pl-4 mb-4'}>
			{text}
		</h2>
	);
};

const Subtitle = ({ text }: { text: string }) => {
	return (
		<h2 className={'mb-8 border-b-2'}>
			{text}
		</h2>
	);
};

const Category = ({ text }: { text: string }) => {
	return (
		<div className={'border-l-4 text-md font-semibold border-green-400 py-1 pl-4 mb-4'}>
			{text}
		</div>
	);
};

const Penjelasan = () => {
	return (
		<>
			<div className={'bg-white '}>
				<div className={'mb-8'}>
					<Title text={'Susunan Kalimat'} />
					<div className={'mb-8'}>
						<Subtitle text={'Bahasa Indonesia'} />
						<div>
							<div className={'mb-8'}>
								<Category text={'Struktur'} />
								<div className={'mb-8'}>
									<div className={'flex'}>
										<div className='bg-red-100 my-4 px-2 font-semibold'>
											{'Fixed: S + P + O + (K)'}
										</div>
									</div>
								</div>
							</div>
							<div className={'mb-8'}>
								<Category text={'Penjelasan'} />
								<div className={'mb-8'}>
									<div className={'flex'}>
										<div className='my-4 text-justify'>
											{'Pada dasar nya , susunan kalimat dalam bahasa Indonesai terdiri atas susunan Subjek + Predikat + Object + (Keterangan).'}
										</div>
									</div>
								</div>
							</div>
							<div className={'mb-8'}>
								<Category text={'Contoh'} />
								<div className={'mb-8'}>
									<div className={'flex'}>
										<div className='my-4 text-justify'>
											{'Bahasa Indonesia (Fixed: S + P + O + (K))'}
										</div>
									</div>
									<div>
										<table className='table-auto w-full'>
											<thead>
												<tr className='bg-green-300 text-sm sm:text-lg border-b-2 border-gray-50'>
													<th className='py-4 border border-gray-50'>
														Subject
													</th>
													<th className='py-4 border border-gray-50'>
														Predikat
													</th>
													<th className='py-4 border border-gray-50'>
														Object
													</th>
													<th className='py-4 border border-gray-50'>
														Keterangan
													</th>
												</tr>
											</thead>
											<tbody className='bg-green-200'>
												<tr className='h-12 text-center text-sm sm:text-lg'>
													<td className='py-2 border border-gray-50'>
														Dewi
													</td>
													<td className='py-2 border border-gray-50'>
														Makan
													</td>
													<td className='py-2 border border-gray-50'>
														Bakso
													</td>
													<td className='py-2 border border-gray-50'>
														<span className='text-red-600'>Di</span> warung
													</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div className={'mb-8'}>
						<Subtitle text={'Bahasa Jepang'} />
						<div>
							<div className={'mb-8'}>
								<Category text={'Struktur'} />
								<div className={'mb-8'}>
									<div className={'flex'}>
										<div className='bg-red-100 my-4 px-2 font-semibold'>
											{'Fleksibel: (unsur 1 + unsur 2 + unsur 3 + ...) + P'}
										</div>
									</div>
								</div>
							</div>
							<div className={'mb-8'}>
								<Category text={'Penjelasan'} />
								<div className={'mb-8'}>
									<div className={'flex'}>
										<div className='my-4 text-justify'>
											{'Susunan kalimat bahasa Jepang fleksibel (bebas), asal predikatnya diletakan di pada akhir kalimat.'}
										</div>
									</div>
								</div>
							</div>
							<div className={'mb-8'}>
								<Category text={'Contoh'} />
								<div className={'mb-8'}>
									<div className={'flex'}>
										<div className='my-4 text-justify'>
											{'Bahasa Jepang (Fleksibel: unsur 1 + unsur 2 + unsur 3 + ... + P)'}
										</div>
									</div>
									<div>
										<table className='table-auto w-full'>
											<thead>
												<tr className='bg-green-300 text-sm sm:text-lg border-b-2 border-gray-50'>
													<th className='py-4 border border-gray-50'>
														Unsur 1
													</th>
													<th className='py-4 border border-gray-50'>
														Unsur 2
													</th>
													<th className='py-4 border border-gray-50'>
														Unsur 3
													</th>
													<th className='py-4 border border-gray-50'>
														Predikat
													</th>
												</tr>
											</thead>
											<tbody className='bg-green-200'>
												<tr className='h-12 text-center text-sm sm:text-lg'>
													<td className='py-2 border border-gray-50'>
														<div>
															Dewi <span className='text-red-600'>wa</span>
														</div>
														<div>
															(Subject)
														</div>
													</td>
													<td className='py-2 border border-gray-50'>
														<div>
															Warung <span className='text-red-600'>de</span>
														</div>
														<div>
															(Tempat)
														</div>
													</td>
													<td className='py-2 border border-gray-50'>
														<div>
															Bakso <span className='text-red-600'>wo</span>
														</div>
														<div>
															(Object)
														</div>
													</td>
													<td rowSpan={6} className='py-2 border border-gray-50'>
														<div>
															tabe-masu.
														</div>
														<div>
															makan
														</div>
														<div>
															(Predikat)
														</div>
													</td>
												</tr>
												<tr className='h-12 text-center text-sm sm:text-lg'>
													<td className='py-2 border border-gray-50'>
														<div>
															Dewi <span className='text-red-600'>wa</span>
														</div>
														<div>
															(Subject)
														</div>
													</td>
													<td className='py-2 border border-gray-50'>
														<div>
															Bakso <span className='text-red-600'>wo</span>
														</div>
														<div>
															(Object)
														</div>
													</td>
													<td className='py-2 border border-gray-50'>
														<div>
															Warung <span className='text-red-600'>de</span>
														</div>
														<div>
															(Tempat)
														</div>
													</td>
												</tr>
												<tr className='h-12 text-center text-sm sm:text-lg'>
													<td className='py-2 border border-gray-50'>
														<div>
															Bakso <span className='text-red-600'>wo</span>
														</div>
														<div>
															(Object)
														</div>
													</td>
													<td className='py-2 border border-gray-50'>
														<div>
															Dewi <span className='text-red-600'>wa</span>
														</div>
														<div>
															(Subject)
														</div>
													</td>
													<td className='py-2 border border-gray-50'>
														<div>
															Warung <span className='text-red-600'>de</span>
														</div>
														<div>
															(Tempat)
														</div>
													</td>
												</tr>
												<tr className='h-12 text-center text-sm sm:text-lg'>
													<td className='py-2 border border-gray-50'>
														<div>
															Bakso <span className='text-red-600'>wo</span>
														</div>
														<div>
															(Object)
														</div>
													</td>
													<td className='py-2 border border-gray-50'>
														<div>
															Warung <span className='text-red-600'>de</span>
														</div>
														<div>
															(Tempat)
														</div>
													</td>
													<td className='py-2 border border-gray-50'>
														<div>
															Dewi <span className='text-red-600'>wa</span>
														</div>
														<div>
															(Subject)
														</div>
													</td>
												</tr>
												<tr className='h-12 text-center text-sm sm:text-lg'>
													<td className='py-2 border border-gray-50'>
														<div>
															Warung <span className='text-red-600'>de</span>
														</div>
														<div>
															(Tempat)
														</div>
													</td>
													<td className='py-2 border border-gray-50'>
														<div>
															Bakso <span className='text-red-600'>wo</span>
														</div>
														<div>
															(Object)
														</div>
													</td>
													<td className='py-2 border border-gray-50'>
														<div>
															Dewi <span className='text-red-600'>wa</span>
														</div>
														<div>
															(Subject)
														</div>
													</td>
												</tr>
												<tr className='h-12 text-center text-sm sm:text-lg'>
													<td className='py-2 border border-gray-50'>
														<div>
															Warung <span className='text-red-600'>de</span>
														</div>
														<div>
															(Tempat)
														</div>
													</td>
													<td className='py-2 border border-gray-50'>
														<div>
															Dewi <span className='text-red-600'>wa</span>
														</div>
														<div>
															(Subject)
														</div>
													</td>
													<td className='py-2 border border-gray-50'>
														<div>
															Bakso <span className='text-red-600'>wo</span>
														</div>
														<div>
															(Object)
														</div>
													</td>
												</tr>
												<tr className='h-12 text-center text-sm sm:text-lg'>
													<th colSpan={4} className='py-2 border border-gray-50'>
														<div>
															Dewi makan bakso di warung
														</div>
													</th>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div className={'mb-8'}>
					<Title text={'Penentuan Unsur Kata Benda dalam Kalimat'} />
					<div className={'mb-8'}>
						<Subtitle text={'Bahasa Indonesia'} />
						<div>
							<div className={'mb-8'}>
								<Category text={'Struktur'} />
								<div className={'mb-8'}>
									<div className={''}>
										<div className='bg-red-100 my-4 px-2 font-semibold'>
											{'Urutan: KB1(S) + KK(P) + KB2(O)'}
										</div>
										<div className='my-4 font-semibold text-sm'>
											{'*KB=kata benda, KK=kata kerja'}
										</div>
									</div>
								</div>
							</div>
							<div className={'mb-8'}>
								<Category text={'Penjelasan'} />
								<div className={'mb-8'}>
									<div className={'flex'}>
										<div className='my-4 text-justify'>
											{'Unsur kata benda seperti subjek dan objek ditentukan oleh urutan kata dalam'}
										</div>
									</div>
								</div>
							</div>
							<div className={'mb-8'}>
								<Category text={'Contoh'} />
								<div className={'mb-8'}>
									<div className={'flex'}>
										<div className='my-4 text-justify'>
											{'Bahasa Indonesia ( KB1(S) + KK(P) + KB2(O) )'}
										</div>
									</div>
									<div>
										<table className='table-auto w-full'>
											<thead>
												<tr className='bg-green-300 text-sm sm:text-lg border-b-2 border-gray-50'>
													<th className='py-4 border border-gray-50'>
														Subject
													</th>
													<th className='py-4 border border-gray-50'>
														Predikat
													</th>
													<th className='py-4 border border-gray-50'>
														Object
													</th>
												</tr>
											</thead>
											<tbody className='bg-green-200'>
												<tr className='h-12 text-center text-sm sm:text-lg'>
													<td className='py-2 border border-gray-50'>
														Dewi
													</td>
													<td className='py-2 border border-gray-50'>
														Makan
													</td>
													<td className='py-2 border border-gray-50'>
														Bakso
													</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div className={'mb-8'}>
						<Subtitle text={'Bahasa Jepang'} />
						<div>
							<div className={'mb-8'}>
								<Category text={'Struktur'} />
								<div className={'mb-8'}>
									<div className={''}>
										<div className='bg-red-100 my-4 px-2 font-semibold'>
											{'Partikel: [KB1 + Partikel][KB2 + Partikel][...] + Predikat'}
										</div>
									</div>
								</div>
							</div>
							<div className={'mb-8'}>
								<Category text={'Penjelasan'} />
								<div className={'mb-8'}>
									<div className={''}>
										<div className='my-4 text-justify'>
											{'Unsur kata benda ditentukan oleh "partikel (sejenis kata bantu)" seperti ga, wa, wo, dan lain-lain, dan tidak tentukan oleh urutan kata dalam kalimat. Fungsi partikel dalam bahasa Jepang mirip "kata depan" dalam bahasa Indonesia. Namun, justru partikel dalam bahasa jepang merupakan "kata belakang" yang dapat menunjukan subjek dan objek juga.'}
										</div>
									</div>
								</div>
							</div>
							<div className={'mb-8'}>
								<Category text={'Contoh'} />
								<div className={'mb-8'}>
									<div className={'flex'}>
										<div className='my-4 text-justify'>
											{'Bahasa Jepang ( [KB1 + Partikel][KB2 + Partikel][...] + KK(Predikat) )'}
										</div>
									</div>
									<div>
										<table className='table-auto w-full'>
											<thead>
												<tr className='bg-green-300 text-sm sm:text-lg border-b-2 border-gray-50'>
													<th className='py-2 border border-gray-50'>
														Unsur 1
													</th>
													<th className='py-2 border border-gray-50'>
														Unsur 2
													</th>
													<th rowSpan={2} className='py-2 border border-gray-50'>
														Predikat
													</th>
												</tr>
												<tr className='bg-green-300 text-sm sm:text-lg border-b-2 border-gray-50'>
													<th className='py-2 border border-gray-50'>
														Subject (Topik)
													</th>
													<th className='py-2 border border-gray-50'>
														Objek
													</th>
												</tr>
											</thead>
											<tbody className='bg-green-200'>
												<tr className='h-12 text-center text-sm sm:text-lg'>
													<td className='py-2 border border-gray-50'>
														Dewi <span className='text-red-600'>wa</span>
													</td>
													<td className='py-2 border border-gray-50'>
														Bakwo <span className='text-red-600'>wo</span>
													</td>
													<td className='py-2 border border-gray-50'>
														tabe-masu.
													</td>
												</tr>
												<tr className='h-12 text-center text-sm sm:text-lg'>
													<td colSpan={3} className='py-2 border border-gray-50'>
														<div>
															Dewi <span className='text-red-600'>wa</span> bakso <span className='text-red-600'>wo</span> tabe-masu.
														</div>
														<div>
															Dewi makan bakso
														</div>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
									<div className={'flex'}>
										<div className='my-4 text-justify'>
											{'Atau'}
										</div>
									</div>
									<div>
										<table className='table-auto w-full'>
											<thead>
												<tr className='bg-green-300 text-sm sm:text-lg border-b-2 border-gray-50'>
													<th className='py-2 border border-gray-50'>
														Unsur 1
													</th>
													<th className='py-2 border border-gray-50'>
														Unsur 2
													</th>
													<th rowSpan={2} className='py-2 border border-gray-50'>
														Predikat
													</th>
												</tr>
												<tr className='bg-green-300 text-sm sm:text-lg border-b-2 border-gray-50'>
													<th className='py-2 border border-gray-50'>
														Objek
													</th>
													<th className='py-2 border border-gray-50'>
														Subject (Topik)
													</th>
												</tr>
											</thead>
											<tbody className='bg-green-200'>
												<tr className='h-12 text-center text-sm sm:text-lg'>
													<td className='py-2 border border-gray-50'>
														Bakwo <span className='text-red-600'>wo</span>
													</td>
													<td className='py-2 border border-gray-50'>
														Dewi <span className='text-red-600'>wa</span>
													</td>
													<td className='py-2 border border-gray-50'>
														tabe-masu.
													</td>
												</tr>
												<tr className='h-12 text-center text-sm sm:text-lg'>
													<td colSpan={3} className='py-2 border border-gray-50'>
														<div>
															bakso <span className='text-red-600'>wo</span> Dewi <span className='text-red-600'>wa</span> tabe-masu.
														</div>
														<div>
															Dewi makan bakso
														</div>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
									<div className={'flex'}>
										<div className='my-4 text-justify'>
											{'Kalimat 1 dan 2 artinya sama, yaitu "Dewi makan bakso" meski pun urutan kata kalimat 1 dan 2 berbeda. Unsur seperti subjek dan objek ditentukan oleh partikel. Dalam contoh kalimat di atas, partikel "wa" menentukan subjek atau topik, sedangkan partikel "wo" menunjukan objek. Partikel-lah menentukan jenis unsur kata benda dalam kalimat bahasa Jepang. (nanti kita pelajari setiap partikel).'}
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</>
	);
};


(PerbedaanIndonesiaJepang as PageWithLayoutType).layout = Main;

export default PerbedaanIndonesiaJepang;