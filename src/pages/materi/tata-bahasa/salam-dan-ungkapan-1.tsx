import React from 'react';
import Head from 'next/head';
import Main from '@com/Layout/Main';
import Link from 'next/link';
import PageWithLayoutType from '@type/layout';
import { BiChevronRight, BiChevronLeft } from 'react-icons/bi';


interface Props {
}

const content = [
	{
		title: 'Salam pertemuan',
		subcontent: [
			{
				title: 'Pagi (saat bangun tidur sampai sekitar 10:00 a.m - 12:00 a.m)',
				data: [
					{
						data: [
							{
								japan: 'おはようございます。',
								romaji: 'Ohayou gozai-masu.',
								indonesia: 'Selamat pagi',
							},
							{
								japan: 'おはよう。',
								romaji: 'Ohayou.',
								indonesia: 'Selamat pagi',
							},
						],
						deskripsi: '"Ohayou gozai-masu" diucapkan pada pagi hari secara umum, sedangkan "ohayou" yang berupa informasi informal dari "ohayou gaza-imasu" digunakan antara teman, anggota keluarga, atau oleh orang yang berpangkat lebih tinggi kepada yang lebih rendah. Orang Indonesia.'
					}
				],
			},
			{
				title: 'Siang,sore (sekitar 10:00 a.m - 12:00 p.m sampai matahari terbenam)',
				data: [
					{
						data: [
							{
								japan: 'こんにちは。',
								romaji: 'Kon\'nichiwa.',
								indonesia: 'Selamat siang/sore.',
							},
						],
						deskripsi: '"Kon\'nichiwa" diucapkan pada siang hari secara umum (tetapi ucapan ini sangat jarang digunakan kepada orang uang sering bertemu sekaligus hubungannya dekat dengan pembicara seperti teman akrab, teman di sekolah, atau anggota keluarga, dan seterusnya).',
					}
				],
			},
			{
				title: 'Malam (setelah matahari tebenam sampai tidur)',
				data: [
					{
						data: [
							{
								japan: 'こんばんは。',
								romaji: 'Konbanwa',
								indonesia: 'Selamat malam.',
							},
						],
						deskripsi: '"Konbanwa" diucapkan pada malam hari setelah matahari terbenam. (Ucapan ini juga jarang digunakan kepada orang yang sering bertemu sekaligus hubungannya dekat dengan pembicara seperti teman akrap, teman di sekolah, atau anggota keluarga, dan seterusnya)',
					},
				],
			},
			{
				title: 'Saat Bekerja',
				data: [
					{
						data: [
							{
								japan: 'おつかれさまです。',
								romaji: 'Otsukaresama desu',
								indonesia: 'Selamat bekerja./ Terima kasih atas usaha dan kerja keras anda.',
							},
						],
						deskripsi: 'Ucapan ini digunakan untuk mengucapkan "selamat bekerja" kepada anggota kantor, rekan bisnis, dan seterusnya pada saat bekerja. Pekerja di kantor Jepang lebih sering menggunakan salam "Otsukaresama desu" dibanding dengan "Kon\'nichiwa", "Konbanwa" dan lain-lain pada saat jam kerja.',
					}
				],

			}
		]
	},
	{
		title: 'Saat berpisah',
		subcontent: [
			{
				title: '',
				data: [
					{
						data: [
							{
								japan: 'さようなら。',
								romaji: 'Sayounara',
								indonesia: 'Sampai jumpa lagi.',
							},
						],
						deskripsi: '"Sayounara" diucapkan untuk salam perpisahan, dan biasa digunakan sekolah (antara guru dengan murid), atau dalam lingkungan tetangga secara rutin setiap hari. Selain itu, ucapan ini digunakan saat berpisah degan seseorang yang kemungkinan besar tidak akan kembali atau bertemu lagi. Ucapan ini tidak sering diucapkan antara teman, sekantor, anggota keluarga, dan seterusnya dalam kehidupan sehari-hari.',
					},
					{
						data: [
							{
								japan: 'じゃあ、（また、あした）。',
								romaji: 'Jaa, (mata ashita).',
								indonesia: 'Bailah, (sampai besok)',
							},
						],
						deskripsi: '"Jaa" atau "Jaa mata ashita" digunakan di antara sederajat seperti teman. Ungkapan ini merupakan bahasa informal dan sering digunakan dalam percakapan sehari-hari.',
					},
					{
						data: [
							{
								japan: 'バイバイ。',
								romaji: 'Bai bai.',
								indonesia: 'Dada',
							},
						],
						deskripsi: '"Bai bai" merupakan kata serapan dari "Bye" dalam bahasa Inggris, dan biasanya diucapkan oleh anak muda. Ungkapan ini juga termasuk bahasa informal dan sering digunakan dalam percakapan sehari-hari.',
					},
					{
						data: [
							{
								japan: 'おやすみなさい。',
								romaji: 'Oyasumi nasai',
								indonesia: 'Selamat tidur.',
							},
						],
						deskripsi: 'Ucapan "Oyasumi nasai" digunakan saat ingin tidur, atau berpisah dengan lawan bicara pada malam hari',
					}
				]
			},
		],
	},
	{
		title: 'Saat ingin mengucapkan rasa terima kasih',
		subcontent: [
			{
				title: '',
				data: [
					{
						data: [
							{
								japan: 'ありがとうございます。',
								romaji: 'Arigatou gozai masu.',
								indonesia: 'Terima kasih (formal).',
							},
							{
								japan: 'ありがとう。',
								romaji: 'Arigatou',
								indonesia: 'Terima kasih (informal)',
							},
						],
						deskripsi: '"Arigatou gozai masu" digunakan untuk mengucapkan "Terima kasih" secara umum dan secara sopan. Sedangkan, "Arigatou" yang juga artinya "Terima kasih" merupakan bahasa formal, dan sering digunakan kepada lawan bicara yang hubungannya dekat dengan pembicara seperti teman, sekeluarga, dan seterusnya.',
					},
					{
						data: [
							{
								japan: 'どういたしまして。',
								romaji: 'dou itashi mashite.',
								indonesia: 'Sama-sama',
							},
						],
						deskripsi: '"Dou itashi mashite" digunakan untuk menyatakan "Terima kasih kembali" sebagai balasan dari "Terima kasih".',
					}
				],
			}
		]
	},
	{
		title: 'Saat ingin minta maaf',
		subcontent: [
			{
				title: '',
				data: [
					{
						data: [
							{
								japan: 'すみません。',
								romaji: 'Sumi-masen.',
								indonesia: 'Minta maaf. (formal)',
							},
						],
						deskripsi: '"Sumi-masen" diucapkan untuk menyatakan "Minta maaf". Ungkapan ini berbentuk sopan dan bersifat umum sehingga dapat digunakan kepada orang umum secara keseluruhan. Hanya saja, sumi-masen tidak dapat digunakan kepada orang-orang yang seharusnya dihormati oleh pembicara seperti tamu, klien, dan seterusnya (biasanya ada hubungan dalam bisnis).'
					},
					{
						data: [
							{
								japan: 'ごめんなさい。',
								romaji: 'Gomen-nasai.',
								indonesia: 'Maaf.',
							},
						],
						deskripsi: 'Ungkapan ini diucapkan untuk minta maaf kepada lawan bicara yang hubunganya dekat dengan pembicara seperti teman, sekeluarga, dan seterusnya. "Gomen-nasai" merupakan bahasa informal.',
					},
					{
						data: [
							{
								japan: 'ごめん。',
								romaji: 'Gomen.',
								indonesia: 'Maaf. (informal)',
							},
						],
						deskripsi: 'Sama halnya "Gomen" sebagai kata singkat dari "gomen-nasai" dapat digunakan untuk meminta maaf. Namun, ungkapan ini lebih informal daripada "Gomen-nasai", dan dapat digunakan kepada sahabat saja.',
					},
					{
						data: [
							{
								japan: 'もうしわけありません。/ もうしわけございません。',
								romaji: 'Moushiwake ari-masen. / Moushiwake gozai-masen.',
								indonesia: 'Mohon maaf. (formal)',
							},
						],
						deskripsi: '"Moushiwake ari-masen(gozai-masen)" digunakan untuk menyatakaan mohon maaf. Ucapan ini yang paling sopan daripada yang telah dijelaskan di atas, dan dapat digunakan kepada orang-orang yang seharusnya dihormati oleh pembicara, seperti tamu, klien, atau orang-orang yang posisinya lebih tinggi daripada pembicara.',
					},
				]
			}
		]
	},
	{
		title: 'Saat makan',
		subcontent: [
			{
				title: '',
				data: [
					{
						data: [
							{
								japan: 'いただきます。',
								romaji: 'Itadaki-masu.',
								indonesia: 'Selamat makan.',
							},
						],
						deskripsi: '"Itadaki-masu" diucapkan sebelum mulai makan (pas saat mulai makan) untuk menyatakan rasa terima kasih kepada seluruh puhak yang telah menyediakan makanan seperti petani, nelayan, pedagang, pemasak, penyedia, tuan tumah, tulang punggung keluarga, sampai bahan-bahan makanan yang telah menyumbangkan nyawanya. secara harfiah, "Itadaki-masu" berarti "Saya menerima".'
					},
					{
						data: [
							{
								japan: 'ごちそうさまでした。',
								romaji: 'Gochisousama-deshita.',
								indonesia: 'Terima kasih atas hidangannya.',
							},
						],
						deskripsi: '"Gochisousama-deshita" diucapkan setelah makan. Ucapan ini juga menyatakan rasa terima kasih kepada seluruh kalangan yang telah menyediakan hidangannya.'
					},
				],
			},
		],
	}

];


const SalamDanUngkapan1: React.FC<Props> = () => {

	const [tap, setTap] = React.useState('PENJELASAN');

	return (
		<>
			<Head>
				<title>{'Tata Bahasa - Salam dan Ungkapan 1'}</title>
				<meta name="og:title" content={'Materi | Tata Bahasa | Salam dan Ungkapan | Salam dan Ungkapan 1 | Belajar Bahasa Jepang Online | kotoba'} />
				<meta name="og:description" content="..." />
			</Head>
			<div className={'p-4 font-sans w-full max-w-4xl mx-auto'}>
				<div className={'w-full text-2xl mb-4'}>
					<div className={'hidden sm:flex  items-center'}>
						<Link href={{ pathname: '/materi' }}>
							<a>
								<h1 className={'mr-2'} >{'Materi'}</h1>
							</a>
						</Link>
						<BiChevronRight className={'mr-2'} size={'1em'} />
						<Link href={{ pathname: '/materi/tata-bahasa' }}>
							<a>
								<h1 className={'mr-2'} >{'Tata Bahasa'}</h1>
							</a>
						</Link>
						<BiChevronRight className={'mr-2'} size={'1em'} />
						<h1 className={'mr-2'} >{'Salam dan Ungkapan 1'}</h1>
					</div>
					<div className={'flex sm:hidden items-center'}>
						<Link href={{ pathname: '/materi/tata-bahasa' }}>
							<a>
								<BiChevronLeft className={'mr-2'} size={'1em'} />
							</a>
						</Link>
						<h1 className={'mr-2'} >{'Salam dan Ungkapan 1'}</h1>
					</div>
				</div>
				<div className={'p-4 shadow rounded-md bg-white font-notoSerif'}>
					<div className={'mb-8'}>
						<p className={'text-justify mb-4'}>
							あいさつ &quot;Aisatsu&quot; dalam bahasa Jepang yang berarti &quot;Salam&quot; dalam bahasa Indonesia terkadang jauh
							lebih dari penting daripada tata bahasa karena fungsi bahasa sebenarnya bukan untuk menyusun
							kalimat, melaikan untuk berkomunikasi dengan orang lain. Biasanya, orang Jepang lebih sering
							menggunakan aisatsu daripada orang Indonesai dalam kehidupan sehari-hari. Misalnya, saat bangun
							tidur, kami biasa mengucapkan &quot;Selamat pagi&quot; kepada orang tua kamu, saudara-saudara, dsb secara
							rutin setiap pagi dalam lingkungan keluarga. Di sekolah, kantor, atau tempat kegiatan yang
							lainya juga begitu. Saat bertemu dengan masing-masing orang pada pertama kali dalam sehari,
							kami sangat biasa mengucapkan &quot;Selamat pagi&quot; kepada setiap orang setiap hari.
						</p>
						<p className={'text-justify mb-4'}>
							Dalam masyarakat Jepang, orang yang biasa mengucapkan aisatsu terkesan positif, sebaliknya
							orang yang jarang mengucapkan aisatsu dapat dianggap negatif. Aisatsu bagai minyak untuk mesin
							biar melancarkan segala hal. Dengan kata lain, orang Jepang mudah menilai sifat dan sikap
							seseorang dari ucapan aisatsu.
						</p>
					</div>
					<div className={''}>
						<div className={'flex mb-8'}>
							<div className={tap === 'PENJELASAN' ? 'p-2 bg-white rounded-t-md border-b-2 border-green-400 font-semibold' : 'p-2'} onClick={() => setTap('PENJELASAN')}>
								Penjelasan
							</div>
						</div>
						<div className={tap === 'PENJELASAN' ? 'block' : 'hidden'}>
							<Penjelasan />
						</div>
					</div>
				</div>
			</div>
		</>
	);
};

const Penjelasan = () => {
	return (
		<>
			{content.map((v, key) => {
				return (
					<div className={'bg-white '} key={key}>
						<h2 className={'border-l-8 border-green-400 py-2 pl-4 mb-4'}>
							{v.title}
						</h2>
						<div>
							{v.subcontent.map((v, i) => {
								return (
									<div className={'mb-8'} key={i}>
										{v.title && (
											<h3 className={'mb-8 border-b-2'}>
												{v.title}
											</h3>
										)}
										<div className={'mb-4'}>
											{v.data.map((v, i) => {
												return (
													<div className={'mb-4'} key={i}>
														<div className={'mb-4'}>
															{v.data.map((v, i) => {
																return (
																	<div className={'mb-4'} key={i}>
																		<div className={'text-xl flex'}>
																			<div className={'bg-yellow-100 mb-2 px-2 font-semibold'}>
																				{v.japan}
																			</div>
																		</div>
																		<div className={'text-xl flex'}>
																			<div className={'bg-green-100 mb-2 px-2 font-semibold'}>
																				{v.romaji}
																			</div>
																		</div>
																		<div className={'text-xl flex'}>
																			<div className={'bg-red-100 mb-2 px-2 font-semibold'}>
																				{v.indonesia}
																			</div>
																		</div>
																	</div>
																);
															})}
														</div>
														<div className={'mb-4'}>
															<p className={'text-justify mb-4 whitespace-pre-wrap'}>
																{v.deskripsi}
															</p>
														</div>
													</div>
												);
											})}
										</div>
									</div>
								);
							})}
						</div>
					</div>
				);
			})}
		</>
	);
};


(SalamDanUngkapan1 as PageWithLayoutType).layout = Main;

export default SalamDanUngkapan1;