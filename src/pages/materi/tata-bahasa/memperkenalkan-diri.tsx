import React from 'react';
import Head from 'next/head';
import Main from '@com/Layout/Main';
import Link from 'next/link';
import PageWithLayoutType from '@type/layout';
import { BiChevronRight, BiChevronLeft } from 'react-icons/bi';


interface Props {
}

const content = [
	{
		title: 'Contoh perkenalan diri',
		subcontent: [],
		example: [
			{
				japan: [
					{
						text: 'はじめまして。',
						className: ''
					},
				],
				romaji: [
					{
						text: 'Hajime mashite.',
						className: ''
					},
				],
				indonesia: [
					{
						text: 'Salam kenal.',
						className: ''
					},
				],
			},
			{
				japan: [
					{
						text: 'きなみまり',
						className: 'underline'
					},
					{
						text: 'です。',
						className: 'text-red-600'
					},
					{
						text: '/',
						className: 'mr-1'
					},
					{
						text: 'といいます。',
						className: 'text-red-600'
					},
				],
				romaji: [
					{
						text: 'Kinami Mari',
						className: 'mr-1 underline'
					},
					{
						text: 'desu.',
						className: 'text-red-600 mr-1'
					},
					{
						text: '/',
						className: 'mr-1'
					},
					{
						text: 'to ii-masu.',
						className: 'text-red-600'
					},
				],
				indonesia: [
					{
						text: 'Nama saya',
						className: 'mr-1'
					},
					{
						text: 'Mari Kinami.',
						className: 'underline'
					},
				],
			},
			{
				japan: [
					{
						text: 'まり',
						className: 'underline'
					},
					{
						text: 'とよんでください。',
						className: 'text-red-600'
					},
				],
				romaji: [
					{
						text: 'Mari',
						className: 'mr-1 underline'
					},
					{
						text: 'to yonde kudasai.',
						className: 'text-red-600 mr-1'
					},
				],
				indonesia: [
					{
						text: 'Panggil saja',
						className: 'mr-1'
					},
					{
						text: 'Mari',
						className: 'underline'
					},
				],
			},
			{
				japan: [
					{
						text: 'おおさか',
						className: 'underline'
					},
					{
						text: 'しゅっしんです。',
						className: 'text-red-600'
					},
				],
				romaji: [
					{
						text: 'Osaka',
						className: 'mr-1 underline'
					},
					{
						text: 'shusshin desu.',
						className: 'text-red-600 mr-1'
					},
				],
				indonesia: [
					{
						text: 'Saya berasal dari',
						className: 'mr-1'
					},
					{
						text: 'Osaka.',
						className: 'underline'
					},
				],
			},
			{
				japan: [
					{
						text: '(ねんれいは)',
						className: ''
					},
					{
						text: '17さい',
						className: 'underline'
					},
					{
						text: 'です。',
						className: 'text-red-600'
					},
				],
				romaji: [
					{
						text: '(Nenrei wa)',
						className: 'mr-1'
					},
					{
						text: '17-sai',
						className: 'mr-1 underline'
					},
					{
						text: 'desu.',
						className: 'text-red-600 mr-1'
					},
				],
				indonesia: [
					{
						text: 'Umur saya',
						className: 'mr-1'
					},
					{
						text: '17 tahun.',
						className: 'underline'
					},
				],
			},
			{
				japan: [
					{
						text: 'こうこうせい',
						className: 'underline'
					},
					{
						text: 'です。',
						className: 'text-red-600'
					},
				],
				romaji: [
					{
						text: 'Koukousei',
						className: 'mr-1 underline'
					},
					{
						text: 'desu.',
						className: 'text-red-600 mr-1'
					},
				],
				indonesia: [
					{
						text: 'Saya',
						className: 'mr-1'
					},
					{
						text: 'siswa SMA.',
						className: 'underline'
					},
				],
			},
			{
				japan: [
					{
						text: 'しゅみ',
						className: ''
					},
					{
						text: 'は',
						className: 'text-red-600'
					},
					{
						text: 'バドミントン',
						className: 'underline'
					},
					{
						text: 'です。',
						className: 'text-red-600'
					},
				],
				romaji: [
					{
						text: 'Shumi',
						className: 'mr-1'
					},
					{
						text: 'wa',
						className: 'text-red-600 mr-1'
					},
					{
						text: 'badominton',
						className: 'mr-1 underline'
					},
					{
						text: 'desu.',
						className: 'text-red-600'
					},
				],
				indonesia: [
					{
						text: 'Hobi saya',
						className: 'mr-1'
					},
					{
						text: 'bulu tangkis',
						className: 'underline'
					},
				],
			},
			{
				japan: [
					{
						text: 'どうぞ、よろしくおねがいします。',
						className: ''
					},
				],
				romaji: [
					{
						text: 'Douzo, yoroshiku onegai shi-masu.',
						className: ''
					},
				],
				indonesia: [
					{
						text: 'Mohon bimbingannya.',
						className: ''
					},
				],
			},
		],
	},
	{
		title: 'Mengawali dan menutup perkenalan diri',
		subcontent: [
			{
				title: '',
				data: [
					{
						data: [
							{
								japan: 'はじめまして。',
								romaji: 'Hajime mashite.',
								indonesia: 'Salam kenal.',
							},
							{
								japan: 'どうぞよろしくおねがいします。',
								romaji: 'Douzo yoroshiku onegai shi-masu.',
								indonesia: 'Mohon Bimbingannya.',
							},
						],
						deskripsi: '"Hajime mashite" diucapkan saat mengawali perkenalan pertama, sedangkan "Douzo yoroshiku onegai shi-masu" dapat digunakan saat mengakhiri perkenalan.'
					}
				]
			},
		],
		example: [],
	},
	{
		title: 'Menyatakan nama	',
		subcontent: [
			{
				title: '',
				data: [
					{
						data: [
							{
								japan: '(Nama) です。/ (Nama) といいます。(siswa / anak muda)',
								romaji: '(Nama) desu. / (Nama) to ii-masu.',
								indonesia: 'Saya (Nama)./ Nama saya (Nama)',
							},
						],
						deskripsi: 'Menyatakan nama diti sangat sederhana, yaitu "nama + desu" saja. Fungsi "desu" seperti verb "be (is, am, are, dll)" dalam bahasa Inggris, dan membentuk predikat secara sopan. "Nama + to ii-masu" yang artinya "nama saya disebut (nama)" juga tepat digunakan untuk menyatakan nama Anda. Saya merekomendasikan ungkapan "(Nama) to ii-masu" digunakan oleh orang asing karena terkadang orang Jepang tidak kenal nama orang asing sehingga tidak mengerti bahwa yang disebut dalam "(Nama) desu" adalah nama orang.'
					}
				]
			},
			{
				title: '',
				data: [
					{
						data: [
							{
								japan: '(Nama) ともうします。(orang dewasa / sudah bekerja)',
								romaji: '(Nama) to moushi-masu.',
								indonesia: 'Nama saya (Nama)',
							},
						],
						deskripsi: 'Jika Anda sudah termasuk orang dewasa, atau sudah bekerja, "(Nama) to moushi-masu" yang paling tepat untuk menyatakan nama Anda. Ungkapan ini lebih sopan den bersikap dewasa daripada "(Nama) desu" dan "(Nama) to ii-masu".'
					}
				]
			},
		],
		example: [
			{
				japan: [
					{
						text: 'あぐす',
						className: 'underline'
					},
					{
						text: 'です。',
						className: 'text-red-600'
					},
					{
						text: '(sopan)',
						className: ''
					},
				],
				romaji: [
					{
						text: 'Agus',
						className: 'mr-1 underline'
					},
					{
						text: 'desu.',
						className: ''
					},
				],
				indonesia: [
					{
						text: 'Saya',
						className: 'mr-1'
					},
					{
						text: 'Agus.',
						className: 'underline'
					},
				],
			},
			{
				japan: [
					{
						text: 'あぐす',
						className: 'underline'
					},
					{
						text: 'といいます。',
						className: 'text-red-600'
					},
					{
						text: '(sopan)',
						className: ''
					},
				],
				romaji: [
					{
						text: 'Agus',
						className: 'mr-1 underline'
					},
					{
						text: 'to ii-masu.',
						className: ''
					},
				],
				indonesia: [
					{
						text: 'Nama saya',
						className: 'mr-1'
					},
					{
						text: 'Agus.',
						className: 'underline'
					},
				],
			},
			{
				japan: [
					{
						text: 'あぐす',
						className: 'underline'
					},
					{
						text: 'ともうします。',
						className: 'text-red-600'
					},
					{
						text: '(sopan)',
						className: ''
					},
				],
				romaji: [
					{
						text: 'Agus',
						className: 'mr-1 underline'
					},
					{
						text: 'to moushi-masu.',
						className: ''
					},
				],
				indonesia: [
					{
						text: 'Nama saya',
						className: 'mr-1'
					},
					{
						text: 'Agus.',
						className: 'underline'
					},
				],
			},
		],
	},
	{
		title: 'Menyatakan nama panggilan',
		subcontent: [
			{
				title: '',
				data: [
					{
						data: [
							{
								japan: '(Nama panggilan) とよんでください。',
								romaji: '(Nama panggilan) to yonde kudasai.',
								indonesia: 'Panggil saja (Nama panggilan)',
							},
						],
						deskripsi: 'Jika nama Anda panjang dan Anda ingin dipanggil degan nama panggilan, gunakanlah "(Nama panggilan) yo yonde kudasai".\n\n\nNamun, biasanya orang Jepang sangat jarang menggunakan frasa "to yonde kudasai" dalam perkenalan diri. Menurut penulis, manfaat dari "to yonde kudasai" adalah pada saat orang uang bernama panjang memberitahukan panggilan saja. Misal, \n\n"Ratna Sari Dewi Soekarno to moushi-masu. Dewi to yonde kudasai."\n\nAtau, langsung saja \n\n"Dewi to moushi-masu" \n\ntanpa menggunakan nama panjang.'
					}
				]
			},
		],
		example: [
			{
				japan: [
					{
						text: 'デウぃ',
						className: 'underline'
					},
					{
						text: 'とよんでください。',
						className: 'text-red-600'
					},
				],
				romaji: [
					{
						text: 'Dewi',
						className: 'mr-1 underline'
					},
					{
						text: 'to yonde kudasai.',
						className: 'text-red-600'
					},
				],
				indonesia: [
					{
						text: 'Panggil saja',
						className: 'mr-1'
					},
					{
						text: 'Dewi.',
						className: 'underline'
					},
				],
			},
		],
	},
	{
		title: 'Menyatakan kewarganegaraan',
		subcontent: [
			{
				title: '',
				data: [
					{
						data: [
							{
								japan: '(Nama negara) じんです。',
								romaji: '(Nama negara)-jin desu.',
								indonesia: 'Saya orang (Nama negara)',
							},
						],
						deskripsi: 'Ungkapan "(Nama negara)-jin desu" digunakan untuk menyatakan kewarganegaraan. Jin artinya orang, dan "nama negara + jin" menyatakan otang dari negara tersebut. Misalnya, Indonesai-jin (orang Indonesai), Nihon-jin (orang Jepang), Amerika-jin (orang Amerika), dan sebagainya.'
					}
				]
			},
		],
		example: [
			{
				japan: [
					{
						text: 'にほん',
						className: 'underline'
					},
					{
						text: 'じん',
						className: 'text-orange-600'
					},
					{
						text: 'です。',
						className: 'text-red-600'
					},
				],
				romaji: [
					{
						text: 'Nihon-',
						className: 'underline'
					},
					{
						text: 'jin',
						className: 'text-orange-600 mr-1 underline'
					},
					{
						text: 'desu.',
						className: 'text-red-600'
					},
				],
				indonesia: [
					{
						text: 'Saya',
						className: 'mr-1'
					},
					{
						text: 'orang',
						className: 'text-orange-600 underline mr-1'
					},
					{
						text: 'Jepang.',
						className: 'underline'
					},
				],
			},
			{
				japan: [
					{
						text: 'アメリカ',
						className: 'underline'
					},
					{
						text: 'じん',
						className: 'text-orange-600'
					},
					{
						text: 'です。',
						className: 'text-red-600'
					},
				],
				romaji: [
					{
						text: 'Amerika-',
						className: 'underline'
					},
					{
						text: 'jin',
						className: 'text-orange-600 mr-1 underline'
					},
					{
						text: 'desu.',
						className: 'text-red-600'
					},
				],
				indonesia: [
					{
						text: 'Saya',
						className: 'mr-1'
					},
					{
						text: 'orang',
						className: 'text-orange-600 underline mr-1'
					},
					{
						text: 'Amerika.',
						className: 'underline'
					},
				],
			},
			{
				japan: [
					{
						text: 'インドネシア',
						className: 'underline'
					},
					{
						text: 'じん',
						className: 'text-orange-600'
					},
					{
						text: 'です。',
						className: 'text-red-600'
					},
				],
				romaji: [
					{
						text: 'Indoneshia-',
						className: 'underline'
					},
					{
						text: 'jin',
						className: 'text-orange-600 mr-1 underline'
					},
					{
						text: 'desu.',
						className: 'text-red-600'
					},
				],
				indonesia: [
					{
						text: 'Saya',
						className: 'mr-1'
					},
					{
						text: 'orang',
						className: 'text-orange-600 underline mr-1'
					},
					{
						text: 'Indonesia.',
						className: 'underline'
					},
				],
			},
		],
	},
	{
		title: 'Menyatakan tempat kelahiran',
		subcontent: [
			{
				title: '',
				data: [
					{
						data: [
							{
								japan: '(Tempat kelahiran) しゅっしんです。',
								romaji: '(Tempat Kelahiran) no shusshin desu.',
								indonesia: 'Saya berasal dari (Tempat kelahiran)',
							},
						],
						deskripsi: 'Ungkapan ini dugunakan untuk menygatakan tempat kelahiran (bisanya nama provinsi, kota, dan seterusnya).',
					}
				]
			},
		],
		example: [
			{
				japan: [
					{
						text: 'おおさか',
						className: 'underline'
					},
					{
						text: 'しゅっしん',
						className: 'text-orange-600'
					},
					{
						text: 'です。',
						className: 'text-red-600'
					},
				],
				romaji: [
					{
						text: 'Osaka',
						className: 'underline mr-1'
					},
					{
						text: 'shusshin',
						className: 'text-orange-600 mr-1'
					},
					{
						text: 'desu.',
						className: 'text-red-600'
					},
				],
				indonesia: [
					{
						text: 'Saya',
						className: 'mr-1'
					},
					{
						text: 'berasal dari',
						className: 'text-orange-600 mr-1'
					},
					{
						text: 'Osaka',
						className: 'underline'
					},
				],
			},
			{
				japan: [
					{
						text: 'パダング',
						className: 'underline'
					},
					{
						text: 'しゅっしん',
						className: 'text-orange-600'
					},
					{
						text: 'です。',
						className: 'text-red-600'
					},
				],
				romaji: [
					{
						text: 'Padang',
						className: 'underline mr-1'
					},
					{
						text: 'shusshin',
						className: 'text-orange-600 mr-1'
					},
					{
						text: 'desu.',
						className: 'text-red-600'
					},
				],
				indonesia: [
					{
						text: 'Saya',
						className: 'mr-1'
					},
					{
						text: 'berasal dari',
						className: 'text-orange-600 mr-1'
					},
					{
						text: 'Padang',
						className: 'underline'
					},
				],
			},
			{
				japan: [
					{
						text: 'インドネシアのパダング',
						className: 'underline'
					},
					{
						text: 'しゅっしん',
						className: 'text-orange-600'
					},
					{
						text: 'です。',
						className: 'text-red-600'
					},
				],
				romaji: [
					{
						text: 'Indonesai no Padang',
						className: 'underline mr-1'
					},
					{
						text: 'shusshin',
						className: 'text-orange-600 mr-1'
					},
					{
						text: 'desu.',
						className: 'text-red-600'
					},
				],
				indonesia: [
					{
						text: 'Saya',
						className: 'mr-1'
					},
					{
						text: 'berasal dari',
						className: 'text-orange-600 mr-1'
					},
					{
						text: 'Padang, Indonesai',
						className: 'underline'
					},
				],
			},
		],
	},
	{
		title: 'Menyatakan usia',
		subcontent: [
			{
				title: '',
				data: [
					{
						data: [
							{
								japan: '(ねんれいは)17さいです。',
								romaji: '(Nenrei wa) 17 sai desu.',
								indonesia: 'Umur saya 17 tahun',
							},
						],
						deskripsi: 'Pola kalimat ini digunakan untuk menyatakan umur.',
					}
				]
			},
		],
		example: [
			{
				japan: [
					{
						text: '17さい',
						className: 'underline'
					},
					{
						text: 'です。',
						className: 'text-red-600'
					},
				],
				romaji: [
					{
						text: 'Juu-nana sai',
						className: 'underline mr-1'
					},
					{
						text: 'desu.',
						className: 'text-red-600'
					},
				],
				indonesia: [
					{
						text: 'Umur saya',
						className: 'mr-1'
					},
					{
						text: '17 tahun',
						className: 'underline'
					},
				],
			},
			{
				japan: [
					{
						text: '24さい',
						className: 'underline'
					},
					{
						text: 'です。',
						className: 'text-red-600'
					},
				],
				romaji: [
					{
						text: 'Nijuu-yon sai',
						className: 'underline mr-1'
					},
					{
						text: 'desu.',
						className: 'text-red-600'
					},
				],
				indonesia: [
					{
						text: 'Umur saya',
						className: 'mr-1'
					},
					{
						text: '24 tahun',
						className: 'underline'
					},
				],
			},
		],
	},
	{
		title: 'Menyatakan status',
		subcontent: [
			{
				title: '',
				data: [
					{
						data: [
							{
								japan: '(Status) です。',
								romaji: '(Status) desu.',
								indonesia: 'Saya (Status)',
							},
						],
						deskripsi: 'Ungkapan ini digunakan untuk menyatakan status atau kedudukan.',
					}
				]
			},
		],
		example: [
			{
				japan: [
					{
						text: 'がくせい',
						className: 'underline'
					},
					{
						text: 'です。',
						className: 'text-red-600'
					},
				],
				romaji: [
					{
						text: 'Gakusai',
						className: 'underline mr-1'
					},
					{
						text: 'desu.',
						className: 'text-red-600'
					},
				],
				indonesia: [
					{
						text: 'Saya',
						className: 'mr-1'
					},
					{
						text: 'siswa',
						className: 'underline'
					},
				],
			},
			{
				japan: [
					{
						text: 'だいがくせい',
						className: 'underline'
					},
					{
						text: 'です。',
						className: 'text-red-600'
					},
				],
				romaji: [
					{
						text: 'Daigakusai',
						className: 'underline mr-1'
					},
					{
						text: 'desu.',
						className: 'text-red-600'
					},
				],
				indonesia: [
					{
						text: 'Saya',
						className: 'mr-1'
					},
					{
						text: 'mahasiswa',
						className: 'underline'
					},
				],
			},
			{
				japan: [
					{
						text: 'かいしゃいん',
						className: 'underline'
					},
					{
						text: 'です。',
						className: 'text-red-600'
					},
				],
				romaji: [
					{
						text: 'Kaishain',
						className: 'underline mr-1'
					},
					{
						text: 'desu.',
						className: 'text-red-600'
					},
				],
				indonesia: [
					{
						text: 'Saya',
						className: 'mr-1'
					},
					{
						text: 'karyawan kantor',
						className: 'underline'
					},
				],
			},
			{
				japan: [
					{
						text: 'もしょく',
						className: 'underline'
					},
					{
						text: 'です。',
						className: 'text-red-600'
					},
				],
				romaji: [
					{
						text: 'Moshoku',
						className: 'underline mr-1'
					},
					{
						text: 'desu.',
						className: 'text-red-600'
					},
				],
				indonesia: [
					{
						text: 'Saya',
						className: 'mr-1'
					},
					{
						text: 'pengangguran',
						className: 'underline'
					},
				],
			},
		],
	},
	{
		title: 'Menyatakan hobi',
		subcontent: [
			{
				title: '',
				data: [
					{
						data: [
							{
								japan: 'しゅみは hobi(Kata benda) です。',
								romaji: 'Shumi wa hobi(Kata benda) desu.',
								indonesia: 'Hobi saya adalah hobi(Kata benda).',
							},
						],
						deskripsi: 'Ungkapan ini digunakan untuk menyatakan hobi.',
					}
				]
			},
		],
		example: [
			{
				japan: [
					{
						text: 'しゅみ',
						className: ''
					},
					{
						text: 'は',
						className: 'text-red-600'
					},
					{
						text: 'テニス',
						className: 'underline'
					},
					{
						text: 'です。',
						className: 'text-red-600'
					},
				],
				romaji: [
					{
						text: 'Shumi',
						className: 'mr-1'
					},
					{
						text: 'wa',
						className: 'text-red-600 mr-1'
					},
					{
						text: 'tenis',
						className: 'underline mr-1'
					},
					{
						text: 'desu.',
						className: 'text-red-600'
					},
				],
				indonesia: [
					{
						text: 'Hobi saya adalah',
						className: 'mr-1'
					},
					{
						text: 'tenis',
						className: 'underline'
					},
				],
			},
			{
				japan: [
					{
						text: 'しゅみ',
						className: ''
					},
					{
						text: 'は',
						className: 'text-red-600'
					},
					{
						text: 'さっかー',
						className: 'underline'
					},
					{
						text: 'です。',
						className: 'text-red-600'
					},
				],
				romaji: [
					{
						text: 'Shumi',
						className: 'mr-1'
					},
					{
						text: 'wa',
						className: 'text-red-600 mr-1'
					},
					{
						text: 'sakkaa',
						className: 'underline mr-1'
					},
					{
						text: 'desu.',
						className: 'text-red-600'
					},
				],
				indonesia: [
					{
						text: 'Hobi saya adalah',
						className: 'mr-1'
					},
					{
						text: 'sepak bola',
						className: 'underline'
					},
				],
			},
			{
				japan: [
					{
						text: 'しゅみ',
						className: ''
					},
					{
						text: 'は',
						className: 'text-red-600'
					},
					{
						text: 'べんきょう',
						className: 'underline'
					},
					{
						text: 'です。',
						className: 'text-red-600'
					},
				],
				romaji: [
					{
						text: 'Shumi',
						className: 'mr-1'
					},
					{
						text: 'wa',
						className: 'text-red-600 mr-1'
					},
					{
						text: 'benkyou',
						className: 'underline mr-1'
					},
					{
						text: 'desu.',
						className: 'text-red-600'
					},
				],
				indonesia: [
					{
						text: 'Hobi saya adalah',
						className: 'mr-1'
					},
					{
						text: 'belajar',
						className: 'underline'
					},
				],
			},
			{
				japan: [
					{
						text: 'しゅみ',
						className: ''
					},
					{
						text: 'は',
						className: 'text-red-600'
					},
					{
						text: 'どくしょ',
						className: 'underline'
					},
					{
						text: 'です。',
						className: 'text-red-600'
					},
				],
				romaji: [
					{
						text: 'Shumi',
						className: 'mr-1'
					},
					{
						text: 'wa',
						className: 'text-red-600 mr-1'
					},
					{
						text: 'dokusho',
						className: 'underline mr-1'
					},
					{
						text: 'desu.',
						className: 'text-red-600'
					},
				],
				indonesia: [
					{
						text: 'Hobi saya adalah',
						className: 'mr-1'
					},
					{
						text: 'belajar',
						className: 'underline'
					},
				],
			},
		],
	},
];

type Kosakata = {
	kana: string;
	romaji: string;
	indonesia: string;
}

const kosakata: Kosakata[] = [
	{ kana: 'インドネシア人', romaji: 'Indonesia-jin', indonesia: 'orang Indonesia', },
	{ kana: 'シンガポール人', romaji: 'Shingapooru-jin', indonesia: 'orang Singapura', },
	{ kana: 'タイ人', romaji: 'Tai-jin', indonesia: 'orang Thailand', },
	{ kana: 'インド人', romaji: 'Indo-jin', indonesia: 'orang India', },
	{ kana: '韓国人 / カンコク人', romaji: 'Kankoku-jin', indonesia: 'orang Korea', },
	{ kana: '中国人 / チュウコク人', romaji: 'Chuugoku-jin', indonesia: 'orang Tiongkok', },
	{ kana: 'アメリカ人', romaji: 'Amerika-jin', indonesia: 'orang Amerika', },
	{ kana: 'イギリス人', romaji: 'Inggris-jin', indonesia: 'orang Ingris', },
	{ kana: 'オランダ人', romaji: 'Oranda-jin', indonesia: 'orang Belanda', },
	{ kana: 'フランス人', romaji: 'Furansu-jin', indonesia: 'orang Prancis', },
	{ kana: 'オーストラリア人', romaji: 'Oosutoraria-jin', indonesia: 'orang Australia', },
	{ kana: 'サウジアラビア人', romaji: 'Saujiarabia-jin', indonesia: 'orang Arab Saudi', },
	{ kana: 'エジプト人', romaji: 'Ejiputo-jin', indonesia: 'orang Mesir', },
	{ kana: 'がくせい', romaji: 'gakusei', indonesia: 'Siswa', },
	{ kana: 'しょうがくせい', romaji: 'shougakusei', indonesia: 'Siswa SD', },
	{ kana: 'ちゅうがくせい', romaji: 'chuugakusei', indonesia: 'Siswa SMP', },
	{ kana: 'こうこうせい', romaji: 'koukousei', indonesia: 'Siswa SMA', },
	{ kana: 'だいがくせい', romaji: 'daigakusei', indonesia: 'Mahasiswa', },
	{ kana: 'かいしゃいん', romaji: 'kaishain', indonesia: 'Pegawai perusahaan', },
	{ kana: 'こうむいん', romaji: 'koumuin', indonesia: 'Pegawai negri', },
	{ kana: 'いしゃ', romaji: 'isha', indonesia: 'Dokter', },
	{ kana: 'うんでんしゃ', romaji: 'undensha', indonesia: 'Sopir', },
	{ kana: 'かんごし', romaji: 'kangoshi', indonesia: 'Perawat', },
	{ kana: 'てんいん', romaji: 'ten\'in', indonesia: 'Pelayan toko', },
	{ kana: 'かしゅ', romaji: 'kashu', indonesia: 'Penyanyi', },
	{ kana: 'しゅふ', romaji: 'shufu', indonesia: 'Ibu rumah tangga', },
	{ kana: 'むしゅく', romaji: 'mushuku', indonesia: 'Pengangur', },
	{ kana: 'りょうり', romaji: 'ryouri', indonesia: 'Masakan', },
	{ kana: 'べんきょう', romaji: 'benkyou', indonesia: 'Belajar', },
	{ kana: 'しごと', romaji: 'shigoto', indonesia: 'Pekerjaan', },
];

const MemperkenalkanDiri: React.FC<Props> = () => {

	const [tap, setTap] = React.useState('PENJELASAN');

	return (
		<>
			<Head>
				<title>{'Tata Bahasa - Memperkenalkan Diri'}</title>
				<meta name="og:title" content={'Materi | Tata Bahasa | Memperkenalkan Diri | Belajar Bahasa Jepang Online | kotoba'} />
				<meta name="og:description" content="..." />
			</Head>
			<div className={'p-4 font-sans w-full max-w-4xl mx-auto'}>
				<div className={'w-full text-2xl mb-4'}>
					<div className={'hidden sm:flex  items-center'}>
						<Link href={{ pathname: '/materi' }}>
							<a>
								<h1 className={'mr-2'} >{'Materi'}</h1>
							</a>
						</Link>
						<BiChevronRight className={'mr-2'} size={'1em'} />
						<Link href={{ pathname: '/materi/tata-bahasa' }}>
							<a>
								<h1 className={'mr-2'} >{'Tata Bahasa'}</h1>
							</a>
						</Link>
						<BiChevronRight className={'mr-2'} size={'1em'} />
						<h1 className={'mr-2'} >{'Memperkenalkan Diri'}</h1>
					</div>
					<div className={'flex sm:hidden items-center'}>
						<Link href={{ pathname: '/materi/tata-bahasa' }}>
							<a>
								<BiChevronLeft className={'mr-2'} size={'1em'} />
							</a>
						</Link>
						<div className={'mr-2'} >{'Memperkenalkan Diri'}</div>
					</div>
				</div>
				<div className={'p-4 shadow rounded-md bg-white font-notoSerif'}>
					<div className={''}>
						<div className={'flex mb-8'}>
							<div className={tap === 'PENJELASAN' ? 'p-2 bg-white rounded-t-md border-b-2 border-green-400 font-semibold' : 'p-2'} onClick={() => setTap('PENJELASAN')}>
								Penjelasan
							</div>
							<div className={tap === 'KOSAKATA' ? 'p-2 bg-white rounded-t-md border-b-2 border-green-400 font-semibold' : 'p-2'} onClick={() => setTap('KOSAKATA')}>
								Kosakata
							</div>
							<div className={tap === 'LATIHAN' ? 'p-2 bg-white rounded-t-md border-b-2 border-green-400 font-semibold' : 'p-2'} onClick={() => setTap('LATIHAN')}>
								Latihan
							</div>
						</div>
						<div className={tap === 'PENJELASAN' ? 'block' : 'hidden'}>
							<Penjelasan />
						</div>
						<div className={tap === 'KOSAKATA' ? 'block' : 'hidden'}>
							<Kosakata />
						</div>
						<div className={tap === 'LATIHAN' ? 'block' : 'hidden'}>
							<Latihan />
						</div>
					</div>
				</div>
			</div>
		</>
	);
};

const Penjelasan = () => {
	return (
		<>
			{content.map((v, key) => {
				return (
					<div className={'bg-white '} key={key}>
						<h2 className={'border-l-8 border-green-400 py-2 pl-4 mb-4'}>
							{v.title}
						</h2>
						<div>
							{v.subcontent.map((v, i) => {
								return (
									<div className={'mb-8'} key={i}>
										{v.title && (
											<h3 className={'mb-8 border-b-2'}>
												{v.title}
											</h3>
										)}
										<div className={'mb-4'}>
											{v.data.map((v, i) => {
												return (
													<div className={'mb-4'} key={i}>
														<div className={'mb-4'}>
															{v.data.map((v, i) => {
																return (
																	<div className={'mb-4'} key={i}>
																		<div className={'text-xl flex'}>
																			<div className={'bg-yellow-100 mb-2 px-2 font-semibold'}>
																				{v.japan}
																			</div>
																		</div>
																		<div className={'text-xl flex'}>
																			<div className={'bg-green-100 mb-2 px-2 font-semibold'}>
																				{v.romaji}
																			</div>
																		</div>
																		<div className={'text-xl flex'}>
																			<div className={'bg-red-100 mb-2 px-2 font-semibold'}>
																				{v.indonesia}
																			</div>
																		</div>
																	</div>
																);
															})}
														</div>
														<div className={'mb-4'}>
															<p className={'text-justify mb-4 whitespace-pre-wrap'}>
																{v.deskripsi}
															</p>
														</div>
													</div>
												);
											})}
										</div>
									</div>
								);
							})}
						</div>
						<div>
							{v.example.map((v, i) => {
								return (
									<div key={i} className={'font-notoSerif font-semibold mb-8'}>
										<div className={'flex mb-2 text-black'}>
											{v.japan.map((v, i) => {
												return (
													<span key={i} className={v.className}>
														{v.text}
													</span>
												);
											})}
										</div>
										<div className={'flex mb-2 text-blue-600'}>
											{v.romaji.map((v, i) => {
												return (
													<span key={i} className={v.className}>
														{v.text}
													</span>
												);
											})}
										</div>
										<div className={'flex mb-2'}>
											{v.indonesia.map((v, i) => {
												return (
													<span key={i} className={v.className}>
														{v.text}
													</span>
												);
											})}
										</div>
									</div>
								);
							})}
						</div>
					</div>
				);
			})}
		</>
	);
};

const Kosakata = () => {
	return (
		<>
			{kosakata.map((v, i) => {
				return (
					<div className={'mb-8 flex flex-col sm:flex-row'} key={i}>
						<div className='mr-2 text-black'>
							{v.kana}
						</div>
						<div className='mr-2 text-blue-600'>
							{v.romaji}
						</div>
						<div className='mr-2'>
							{v.indonesia}
						</div>
					</div>
				);
			})}
		</>
	);
};

const Latihan = () => {
	return (
		<>
			<div className={'flex w-full justify-center'}>
				<div className={'my-48'}>
					Latihan Comming Soon
				</div>
			</div>
		</>
	);
};


(MemperkenalkanDiri as PageWithLayoutType).layout = Main;

export default MemperkenalkanDiri;