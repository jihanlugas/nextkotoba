import Head from 'next/head';
import Main from '@com/Layout/Main';
import Link from 'next/link';
import PageWithLayoutType from '@type/layout';
import { BiChevronRight, BiChevronLeft } from 'react-icons/bi';


interface Props {
}

const materi = [
	{
		title: 'Introduksi',
		ruby: 'イントロダクション',
		rt: '',
		sub: [
			{
				title: 'Salam dan Ungkapan 1',
				path: '/materi/tata-bahasa/salam-dan-ungkapan-1',
			},
			{
				title: 'Salam dan Ungkapan 2',
				path: '/materi/tata-bahasa/salam-dan-ungkapan-2',
			},
			{
				title: 'Memperkenalkan Diri',
				path: '/materi/tata-bahasa/memperkenalkan-diri',
			},
			{
				title: 'Perbedaan Bahasa Indonesia dan Bahasa Jepang',
				path: '/materi/tata-bahasa/perbedaan-bahasa-indonesia-dengan-jepang',
			},
			{
				title: 'Huruf Jepang: Hiragana / Katakana, dan Pelafalan Bahasa Jepang',
				path: '/materi/tata-bahasa/huruf-jepang-hiragana-katakana-dan-pelafalan-bahasa-jepang',
			},
			{
				title: 'Cara Menulis Huruf Hiragana dan Katakana',
				path: '/materi/tata-bahasa/cara-menulis-huruf-hiragan-dan-katakana',
			},
			{
				title: 'Struktur Kalimat dalam Bahasa Jepang',
				path: '/materi/tata-bahasa/struktur-kalimat-dalam-bahasa-jepang',
			},
			{
				title: 'Tips dan Cara Menghafal Huruf Hiragana dan Katakana',
				path: '/materi/tata-bahasa/tips-dan-cara-menghafal-huruf-hiragana-dan-katakana',
			},
			{
				title: 'Angka dalam Bahasa Jepang',
				path: '/materi/tata-bahasa/angka-dalam-bahasa-jepang',
			},
		],
	},
	{
		title: 'Pola Kalimat Dasar',
		ruby: '基礎文系',
		rt: 'きそぶんけい',
		sub: [
			{
				title: 'KB1 wa KB2 desu KB1 「は」 KB2 「です」',
				path: '/materi/tata-bahasa/kb1-wa-kb2-desu',
			},
			{
				title: 'Partikel 「も」 : KB1 mo KB2 desu KB1 「も」 KB2 「です」',
				path: '/materi/tata-bahasa/salam-dan-ungkapan-2',
			},
			{
				title: 'Bentuk Negatif: Dewa (Ja) ari-masen 「では（じゃ）ありません」',
				path: '/materi/tata-bahasa/salam-dan-ungkapan-2',
			},
			{
				title: 'Bentuk Tanya: KB1 wa KB2 desu ka 「KB1 は KB2 ですか」',
				path: '/materi/tata-bahasa/salam-dan-ungkapan-2',
			},
			{
				title: '"Hai, sou desu", "Iie, chigai-masu" 「はい、そうです」「いいえ、ちがいます」',
				path: '/materi/tata-bahasa/salam-dan-ungkapan-2',
			},
			{
				title: 'Menanyakan dan Menyatakan Waktu: Ima, nanji desuka 「いま、なんじですか」',
				path: '/materi/tata-bahasa/salam-dan-ungkapan-2',
			},
			{
				title: 'Predikat1 desu ka, Predikat2 desu ka 「〜か、〜か」',
				path: '/materi/tata-bahasa/salam-dan-ungkapan-2',
			},
			{
				title: 'Partikel No 「の」: KB1 no KB2 「KB1のKB2」',
				path: '/materi/tata-bahasa/salam-dan-ungkapan-2',
			},
			{
				title: 'Partikel To 「と」: KB1 to KB2 「KB1とKB2」',
				path: '/materi/tata-bahasa/salam-dan-ungkapan-2',
			},
		],
	},
];

const Index: React.FC<Props> = () => {
	return (
		<>
			<Head>
				<title>{'Materi - Tata Bahasa'}</title>
				<meta name="og:title" content={'Materi | Tata Bahasa | Belajar Bahasa Jepang Online | kotoba'} />
				<meta name="og:description" content="..." />
			</Head>
			<div className={'p-4 font-sans w-full max-w-4xl mx-auto'}>
				<div className={'w-full text-2xl mb-4'}>
					<div className={'hidden sm:flex  items-center'}>
						<Link href={{ pathname: '/materi' }}>
							<a>
								<h1 className={'mr-2'} >{'Materi'}</h1>
							</a>
						</Link>
						<BiChevronRight className={'mr-2'} size={'1em'} />
						<h1 className={'mr-2'} >{'Tata Bahasa'}</h1>
					</div>
					<div className={'flex sm:hidden items-center'}>
						<Link href={{ pathname: '/materi' }}>
							<a>
								<BiChevronLeft className={'mr-2'} size={'1em'} />
							</a>
						</Link>
						<h1 className={'mr-2'} >{'Tata Bahasa'}</h1>
					</div>
				</div>
				<div className={'p-4 shadow rounded-md bg-white font-notoSerif'}>
					<div className={'mb-4 text-justify'}>
						<p className={'font-notoSerif'}>
							Dalam daftar materi ini, kita akan mempelajari tata bahasa Jepang dari nol sampai JLPT N4 atau bahkan lebih dari itu.
							Materi - materi dibawah ini sudah merangkup hampir semua materi tata bahasa yang ada dalam buku <span className={'italic'}>{'"Minna No Nohongo"'}</span> Tingkat Dasar I dan II (bab 1 sampai 50)
						</p>
					</div>
					<div>
						{materi.map((data, key) => {
							return (
								<div key={key} className={'mb-4'}>
									<h2 className={'border-l-8 border-green-400 py-2 pl-4 mb-4'}>
										{data.title} <ruby>{data.ruby}<rt>{data.rt}</rt></ruby>
									</h2>
									<div className={'pl-6 mb-4'}>
										{data.sub.map((v, i) => {
											return (
												<Link key={i} href={{ pathname: v.path ? v.path : '' }} passHref>
													<div className={'mb-4 text-green-500 cursor-pointer'}>
														<h3 className={'text-base'}>{v.title}</h3>
													</div>
												</Link>
											);
										})}
									</div>
								</div>
							);
						})}
					</div>
				</div>
			</div>
		</>
	);
};


(Index as PageWithLayoutType).layout = Main;

export default Index;