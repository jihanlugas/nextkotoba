import Head from 'next/head';
import Main from '@com/Layout/Main';
import Link from 'next/link';
import PageWithLayoutType from '@type/layout';
import { BiChevronRight, BiChevronLeft } from 'react-icons/bi';


interface Props {
}

const Index: React.FC<Props> = () => {
	return (
		<>
			<Head>
				<title>{'Materi - Kosakata'}</title>
			</Head>
			<div className={'p-4 font-sans w-full max-w-4xl mx-auto'}>
				<div className={'w-full text-2xl mb-4'}>
					<div className={'hidden sm:flex  items-center'}>
						<Link href={{ pathname: '/materi' }}>
							<a>
								<div className={'mr-2'} >{'Materi'}</div>
							</a>
						</Link>
						<BiChevronRight className={'mr-2'} size={'1em'} />
						<div className={'mr-2'} >{'Kosakata'}</div>
					</div>
					<div className={'flex sm:hidden items-center'}>
						<Link href={{ pathname: '/materi' }}>
							<a>
								<BiChevronLeft className={'mr-2'} size={'1em'} />
							</a>
						</Link>
						<div className={'mr-2'} >{'Kosakata'}</div>
					</div>
				</div>
				<div className={'p-4 shadow rounded-md bg-white font-notoSerif'}>
					<div className={'my-24 text-center'}>
						Comming soon
					</div>
				</div>
			</div>
		</>
	);
};


(Index as PageWithLayoutType).layout = Main;

export default Index;