import Main from '@com/Layout/Main';
import Head from 'next/head';
import Link from 'next/link';
import type { GetServerSideProps, NextPage, NextPageContext } from 'next';
import React, { useState, ChangeEvent, useEffect, useContext } from 'react';
import { useDebounce } from '@utils/Hooks';
import { isEmptyObject } from '@utils/Validate';
import { Api } from '@lib/Api';
import { useMutation } from 'react-query';
import NotifContext from '@stores/notifContext';
import { AiOutlineLoading3Quarters } from 'react-icons/ai';
import { RiBookmark3Line } from 'react-icons/ri';
import { useRouter } from 'next/router';
import { getRandomKanji } from '@utils/Helper';
import PageWithLayoutType from '@type/layout';
import Example from '@com/View/Example';
import OnReading from '@com/View/OnReading';
import KunReading from '@com/View/KunReading';
import NameReading from '@com/View/NameReading';
import UserContext from '@stores/userProvider';

interface Props {
	qKanji?: string
}

interface ImskanjiExample {
	mskanjiexampleId?: number,
	mskanjiId?: number,
	kanjiexample?: string,
	kana?: string,
	mean?: string,
	description?: string
}

interface Ikanji {
	mskanjiId?: number,
	kanji?: string,
	grade?: string,
	jlpt?: string,
	unicode?: string,
	heisigEn?: string,
	strokeCount?: string,
	listMeaning?: string[],
	listKunReading?: string[],
	listOnReading?: string[],
	listNameReading?: string[],
	listMskanjiExample?: ImskanjiExample[],
}


const Search: React.FC<Props> = ({ qKanji = '' }) => {
	const { notif } = useContext(NotifContext);
	const { user, setUser } = useContext(UserContext);
	const [search, setSearch] = useState<string>(qKanji);
	const [randomKanji, setRandomKanji] = useState<string>(getRandomKanji());
	const debounceSearch = useDebounce(search, 1000);
	const [kanji, setKanji] = useState<Ikanji>({});
	const router = useRouter();

	const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
		setSearch(event.target.value);
	};

	const handlePressExample = (kanji: string) => {
		setSearch(kanji);
	};

	const handleSavekanji = (mskanjiId: number) => {
		mutateAdd({ mskanjiId: mskanjiId }, {
			onSuccess: (res) => {
				if (res) {
					if (res.success) {
						notif.success(res.message);
					} else if (res.error) {
						notif.error(res.message);
					}
				}
			},
			onError: (res) => {
				notif.error('Please cek you connection');
			}
		});
	};

	const { data: dataKanji, mutate: mutateKanji, isLoading } = useMutation((val: { kanji: string }) => Api.get('/mskanji/search', val));
	const { data: dataAdd, mutate: mutateAdd, isLoading: isLoadingAdd } = useMutation((val: { mskanjiId: number }) => Api.get(`/mskanji/addtokanji/${val.mskanjiId}`, {}));


	useEffect(() => {
		setRandomKanji(getRandomKanji());
		if (dataKanji && dataKanji.success) {
			setKanji(dataKanji.payload);
		} else {
			if ((dataKanji && dataKanji.error)) {
				notif.error(dataKanji.message);
			}
			setKanji({});
		}
	}, [dataKanji]);

	useEffect(() => {
		if (debounceSearch === '') {
			router.push({ pathname: '/search' }, undefined, { shallow: true });
			setKanji({});
		} else {
			router.push({ pathname: '/search', query: { kanji: debounceSearch } }, undefined, { shallow: true });
			mutateKanji({ kanji: debounceSearch });
		}
	}, [debounceSearch]);

	return (
		<>
			<Head>
				<title>{'Search'}</title>
				<meta name="og:title" content="Search | Search Kanji | Belajar Bahasa Jepang Online | kotoba" />
				<meta name="og:description" content="..." />
			</Head>
			<div className={'p-4 font-sans w-full max-w-4xl mx-auto'}>
				<div className={'mb-4'}>
					<div className={'flex justify-between items-center'}>
						<h1 className={''} >
							Search
						</h1>
						{user && !isEmptyObject(kanji) && (
							<div>
								{isLoadingAdd ? (
									<AiOutlineLoading3Quarters className={'animate-spin'} size={'1.5em'} />
								) : (
									<div onClick={() => handleSavekanji(kanji.mskanjiId)} className={'cursor-pointer'}>
										<RiBookmark3Line className={''} size={'1.5em'} />
									</div>
								)}
							</div>
						)}
					</div>
					<input
						type="text"
						value={search}
						onChange={handleChange}
						className={'w-full border-2 rounded h-10 px-2 bg-gray-50 font-notoSerif'}
						placeholder={'Search a kanji'}
					/>
				</div>
				<div className={'mb-4'}>
					{isLoading ? (
						<div className={'w-full h-60 flex justify-center items-center'}>
							<AiOutlineLoading3Quarters className={'animate-spin font-bold'} size={'4em'} />
						</div>
					) : (
						<div>
							{isEmptyObject(kanji) && debounceSearch === '' ? (
								<div>
									<div className={'mb-4'}>
										<div>To do search</div>
										<div className={'flex'}>
											<div className={'text-blue-600 cursor-pointer flex items-center'} onClick={() => handlePressExample(randomKanji)} >
												<span className={'mr-2'}>{'Example :'}</span>
												<span className={'font-notoSerif'}>{randomKanji}</span>
											</div>
										</div>
									</div>
									<div className={'mb-4'}>
										<div>Setelah kanji di temukan, tekan icon bookmark di kanan atas untuk menyimpan kanji </div>
									</div>
								</div>
							) : isEmptyObject(kanji) && debounceSearch !== '' ? (
								<div className={'w-full h-40 flex justify-center items-center'}>
									Data Not Found
								</div>
							) : (
								<div>
									<div className={'flex mb-4'}>
										<div className="shrink-0 h-20 w-20 sm:h-28 sm:w-28">
											<div className={'text-5xl sm:text-6xl font-yujiSyuku select-none h-full w-full flex justify-center items-center bg-gray-50 rounded-lg shadow-md animate-fadeIn'}>
												<h1 className={'text-5xl'}>{kanji.kanji}</h1>
											</div>
										</div>
										<div className="flex-1 h-20 grid grid-cols-3 gap-1 sm:h-28">
											<div className={'flex justify-center items-center flex-col'}>
												<div className={'font-semibold text-lg'}>
													{kanji.jlpt}
												</div>
												<div className={'font-light text-sm'}>
													<h3>Jlpt</h3>
												</div>
											</div>
											<div className={'flex justify-center items-center flex-col'}>
												<div className={'font-semibold text-lg'}>
													{kanji.grade}
												</div>
												<div className={'font-light text-sm'}>
													<h3>Grade</h3>
												</div>
											</div>
											<div className={'flex justify-center items-center flex-col'}>
												<div className={'font-semibold text-lg'}>
													{kanji.strokeCount}
												</div>
												<div className={'font-light text-sm'}>
													<h3>Stroke</h3>
												</div>
											</div>
										</div>
									</div>
									<div className={'mb-2 border-b-2'}>
										<div className={'mb-2'}>
											<div className={'text-lg'}>
												<h3>Heisig En</h3>
											</div>
											<div className={'capitalize'}>{kanji.heisigEn}</div>
										</div>
									</div>
									<div className={'mb-2 border-b-2'}>
										<div className={'mb-2'}>
											<div className={'text-lg'}>
												<h3>Meaning</h3>
											</div>
											{kanji.listMeaning.map((meaning, key) => {
												return (
													<div key={key} className={'capitalize'}>
														{(key + 1) + '. ' + meaning}
													</div>
												);
											})}
										</div>
									</div>
									<div className={'mb-2 border-b-2'}>
										<div className={'mb-2'}>
											<div className={'text-lg mb-2'}>
												<h3>On Readings</h3>
											</div>
											<div className={'flex flex-wrap'}>
												{kanji.listOnReading.length !== 0 ? kanji.listOnReading.map((onReading, key) => {
													return (
														<OnReading key={key} data={onReading} />
													);
												}) : <div>-</div>}
											</div>
										</div>
									</div>
									<div className={'mb-2 border-b-2'}>
										<div className={'mb-2'}>
											<div className={'text-lg mb-2'}>
												<h3>Kun Readings</h3>
											</div>
											<div className={'flex flex-wrap'}>
												{kanji.listKunReading.length !== 0 ? kanji.listKunReading.map((kunReading, key) => {
													return (
														<KunReading key={key} data={kunReading} />
													);
												}) : <div>-</div>}
											</div>
										</div>
									</div>
									<div className={'mb-2 border-b-2'}>
										<div className={'mb-2'}>
											<div className={'text-lg mb-2'}>
												<h3>Name Readings</h3>
											</div>
											<div className={'flex flex-wrap'}>
												{kanji.listNameReading.length !== 0 ? kanji.listNameReading.map((nameReading, key) => {
													return (
														<NameReading key={key} data={nameReading} />
													);
												}) : <div>-</div>}
											</div>
										</div>
									</div>
									<div className={'mb-2 border-b-2'}>
										<div className={'mb-2'}>
											<div className={'text-lg mb-2'}>
												<h3>Kanji Example</h3>
											</div>
										</div>
										{kanji.listMskanjiExample && kanji.listMskanjiExample.length > 0 ? kanji.listMskanjiExample.map((mskanjiExample, key) => {
											return (
												<Example
													key={key}
													kana={mskanjiExample.kana}
													kanji={mskanjiExample.kanjiexample}
													mean={mskanjiExample.mean}
													description={mskanjiExample.description}
												/>
											);
										}) : (
											<div>-</div>
										)}
									</div>

								</div>
							)}
						</div>
					)}
				</div>
			</div>
		</>

	);
};

(Search as PageWithLayoutType).layout = Main;

export const getServerSideProps: GetServerSideProps = async (context) => {
	const kanji = context.query.kanji ? context.query.kanji : '';

	return {
		props: {
			qKanji: kanji
		}
	};
};

export default Search;