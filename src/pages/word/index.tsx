import Main from '@com/Layout/Main';
import { Api } from '@lib/Api';
import Head from 'next/head';
import React, { useContext, useEffect, useState } from 'react';
import { useQuery, useInfiniteQuery } from 'react-query';
import { AiOutlineLoading3Quarters } from 'react-icons/ai';
import { useRouter } from 'next/router';
import PageWithLayoutType from '@type/layout';
import UserContext from '@stores/userProvider';
import ModalWord from '@com/Modal/ModalWord';
import Word from '@com/View/Word';

interface Imsword {
	mswordId: number,
	listMsword: string[],
	word: string,
	kana: string,
	mean: string,
	description: string,
}

interface Props {
}

interface IpageOption {
	limit: number,
	page: number,
}

const defaultPage = {
	limit: 12,
	page: 1,
};

const Index: React.FC<Props> = () => {
	const { user, setUser } = useContext(UserContext);
	const [pageOption, setPageoption] = useState<IpageOption>(defaultPage);
	const [mswords, setMswords] = useState<Imsword[]>([]);
	const [showModal, setShowModal] = useState<boolean>(false);
	const [selectedId, setSelectedId] = useState<number>(0);

	const {
		isLoading,
		refetch,
		status,
		data,
		error,
		isFetching,
		isFetchingNextPage,
		fetchNextPage,
		hasNextPage,
	} = useInfiniteQuery(
		'kanji',
		({ pageParam = 1 }) => Api.get('/msword', { page: pageParam, limit: 6 }),
		{
			getNextPageParam: lastPage => lastPage.payload?.page && lastPage.payload.page !== lastPage.payload.totalPage ? lastPage.payload.page + 1 : false,
		}
	);

	const router = useRouter();

	// useEffect(() => {
	// 	if (data && data.success) {
	// 		setMswords(data.payload && data.payload.list ? data.payload.list : []);
	// 	}
	// }, [data]);

	useEffect(() => {
		if (user) {
			refetch();
		}
	}, [user]);

	const handleClickWord = (mswordId: number) => {

	};

	const handleAddPage = () => {
		refetch();
		// setPageoption({ ...pageOption, page: pageOption.page + 1 });
	};

	const toogleModal = (id = 0, refresh = false) => {
		setSelectedId(id);
		setShowModal(!showModal);
		if (refresh) {
			refetch();
		}
	};


	return (
		<>
			<Head>
				<title>Word</title>
				<title>{'Word'}</title>
				<meta name="og:title" content={'Word | Belajar Bahasa Jepang Online | kotoba'} />
				<meta name="og:description" content="..." />
			</Head>
			<div className={'p-4 font-sans w-full max-w-4xl mx-auto'}>
				<div className={'mb-4'}>
					<div className={'mb-4 flex justify-between items-center'}>
						<h1 className={'mr-2'} >
							{'Word'}
						</h1>
						{user && user.userId !== 0 && (
							<div>
								<button className={'bg-green-500 h-10 rounded-md text-gray-50 font-semibold px-4 w-full shadow-lg shadow-green-500/50'} onClick={() => toogleModal(0)}>
									Create
								</button>
							</div>
						)}
					</div>
				</div>
				{user === null ? (
					<div className={'mb-4'}>
						<div>
							Please Login to access this page
						</div>
					</div>
				) : !user.userId ? (
					<div className={'mb-4'}>
					</div>
				) : (
					<>
						<ModalWord onClickOverlay={toogleModal} show={showModal} selectedId={selectedId} />
						{/* <div onClick={handleAddPage}>
							handleAddPage
						</div>
						<div onClick={() => fetchNextPage()}>
							{'hasNextPage ' + hasNextPage}
						</div> */}
						<div className={'mb-4'}>
							{isLoading ? (
								<div className={'w-full h-60 flex justify-center items-center'}>
									<AiOutlineLoading3Quarters className={'animate-spin font-bold'} size={'4em'} />
								</div>
							) : (
								<div>
									{data && data.pages.length > 0 ? (
										<>
											<div className="grid gap-4 grid-cols-1 sm:grid-cols-2 md:grid-cols-3">
												{data.pages.map((page, key) => {
													return (
														<>
															{page?.payload?.list && page.payload.list.map((msword: Imsword, key) => {
																return (
																	<div key={key} onClick={() => handleClickWord(msword.mswordId)}>
																		<Word
																			key={key}
																			mswordId={msword.mswordId}
																			listMsword={msword.listMsword}
																			word={msword.word}
																			kana={msword.kana}
																			mean={msword.mean}
																			description={msword.description}
																		/>
																	</div>
																);
															})}
														</>
													);
												})}
											</div>
											{hasNextPage && (
												<>
													{isFetchingNextPage ? (
														<div className={'w-full h-20 flex justify-center items-center'}>
															<AiOutlineLoading3Quarters className={'animate-spin font-bold'} size={'2em'} />
														</div>
													) : (
														<div className={'w-full h-20 flex justify-center items-center'}>
															<div onClick={() => fetchNextPage()}>
																Fetch Next
															</div>
														</div>
													)}
												</>
											)}
										</>
									) : (
										<div>
											<div>Tidak ada data</div>
										</div>
									)}
								</div>
							)}
						</div>
					</>
				)}
			</div>
		</>
	);

	if (user === null) {
		return (
			<>
				<Head>
					<title>Word</title>
				</Head>
				<div className={'p-4 font-sans w-full max-w-4xl mx-auto'}>
					<div className={'mb-4'}>
						<div className={'mb-4 flex justify-between items-center'}>
							<div className={'text-2xl'} >
								Word
							</div>
						</div>
					</div>
					<div className={'mb-4'}>
						<div>
							Please Login to access this page
						</div>
					</div>
				</div>
			</>
		);
	} else {
		return (
			<>
				<Head>
					<title>Word</title>
				</Head>
				<div className={'p-4 font-sans w-full max-w-4xl mx-auto'}>
					<div className={'mb-4'}>
						<div className={'mb-4 flex justify-between items-center'}>
							<div className={'text-2xl'} >
								Word
							</div>
							<div>
								<button className={'bg-green-500 h-10 rounded-md text-gray-50 font-semibold px-4 w-full shadow-lg shadow-green-500/50'} onClick={() => toogleModal(0)}>
									Create
								</button>
							</div>
						</div>
					</div>
					<ModalWord onClickOverlay={toogleModal} show={showModal} selectedId={selectedId} />
					<div className={'mb-4'}>
						{isLoading ? (
							<div className={'w-full h-60 flex justify-center items-center'}>
								<AiOutlineLoading3Quarters className={'animate-spin font-bold'} size={'4em'} />
							</div>
						) : (
							<div>
								{mswords.length > 0 ? (
									<div className="grid gap-4 grid-cols-1 sm:grid-cols-2 md:grid-cols-3">
										{mswords.map((msword, key) => {
											return (
												<div key={key} className={'bg-white shadow-md rounded-md animate-fadeIn hover:bg-gray-200 p-2 select-none font-notoSerif'} onClick={() => handleClickWord(msword.mswordId)}>
													<div className={'text-lg font-semibold mb-2'}>
														<ruby>
															{msword.word}<rt className={''}>{msword.kana}</rt>
														</ruby>
													</div>
													<div className=' mb-2'>
														{msword.mean}
													</div>
													<div className={'whitespace-pre-wrap text-sm'}>
														{msword.description}
													</div>
												</div>
											);
										})}
									</div>
								) : (
									<div>
										<div>Tidak ada data</div>
									</div>
								)}
							</div>
						)}
					</div>
				</div>
			</>
		);
	}
};

(Index as PageWithLayoutType).layout = Main;

export default Index;