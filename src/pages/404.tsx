import React from 'react';
import PageWithLayoutType from '@type/layout';
import Main from '@com/Layout/Main';

interface Props {
	errorCode?: number;
	title?: string;
}

const Custom404: React.FC<Props> = ({ errorCode, title }) => {
	return (
		<div className={'fixed h-screen w-screen flex flex-col justify-center items-center'}>
			<div className='text-6xl mb-4'>404</div>
			<div className='text-lg'>
				{title ? title : 'Page not found'}
			</div>
		</div>
	);
};

(Custom404 as PageWithLayoutType).layout = Main;

export default Custom404;

