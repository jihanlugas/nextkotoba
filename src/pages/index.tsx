import Main from '@com/Layout/Main';
import PageWithLayoutType from '@type/layout';
import React from 'react';

interface Props {

}

const Index: React.FC<Props> = ({ }) => {
	return (
		<>
			<div>Index</div>
		</>

	);
};

(Index as PageWithLayoutType).layout = Main;

export default Index;