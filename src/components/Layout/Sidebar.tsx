import React, { Fragment, useContext, useEffect, useState } from 'react';
import type { NextPage } from 'next';
import Link from 'next/link';
import { GoThreeBars } from 'react-icons/go';
import { useRouter } from 'next/router';

type Props = {
	onClickOverlay: Function,
	show: boolean,
};

type MenuItemProps = {
	menu: {
		path: string,
		name: string,
	},
	isSelect: boolean,
}

const initAuth = [
	{
		path: '/home',
		name: 'Home',
	},
	{
		path: '/search',
		name: 'Search',
	},
	{
		path: '/mskanji',
		name: 'Master Kanji',
	},
	{
		path: '/kanji',
		name: 'Kanji',
	},
	{
		path: '/word',
		name: 'Word',
	},
	{
		path: '/kana',
		name: 'Kana',
	},
	{
		path: '/materi',
		name: 'Materi',
	},
];


const Sidebar: React.FC<Props> = ({ children, onClickOverlay, show }) => {
	const [authMenu, setAuthMenu] = useState(initAuth);
	const router = useRouter();


	// to close sidebar when pathname has changes
	React.useEffect(() => {
		onClickOverlay(false);
	}, [router.pathname]);


	return (
		<div className={show ? 'z-20 inset-0 overflow-y-auto fixed' : 'hidden'} >
			<div className="min-h-screen text-center p-0">
				<div className="fixed inset-0 transition-opacity" onClick={() => onClickOverlay()} aria-hidden="true">
					<div className="absolute inset-0 bg-gray-500 opacity-75"></div>
				</div>
				<div className="fixed bg-white h-screen flex w-80">
					<div className={'flex bg-white flex-col w-full'}>
						<div className="h-16 flex justify-center items-center fixed w-80 shadow">
							<span className="text-3xl">{process.env.APP_NAME}</span>
						</div>
						<div className="mt-16 h-screen">
							{authMenu.map((menu, index) => {
								const isSelected = router.pathname.indexOf(menu.path) !== -1;
								return (
									<MenuItem menu={menu} isSelect={isSelected} key={index} />
								);
							})}
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};



const MenuItem: React.FC<MenuItemProps> = ({ menu, isSelect }) => {
	return (
		<Link href={menu.path}>
			<a>
				<div className={`p-4 hover:bg-gray-200 w-full flex items-center ${isSelect && 'bg-gray-200'}`}>
					<div className={'flex justify-center items-center h-4 w-4 mr-2'}>
						<GoThreeBars size={'2em'} />
					</div>
					<div>
						<span>{menu.name}</span>
					</div>
				</div>
			</a>
		</Link>
	);
};

export default Sidebar;