import { useContext, useState, useRef, useEffect } from 'react';
import type { NextPage } from 'next';
import { useRouter } from 'next/router';
import { GoThreeBars } from 'react-icons/go';
import { useMutation } from 'react-query';
import { Api } from '@lib/Api';
import Router from 'next/router';
import NotifContext from '@stores/notifContext';
import { AiOutlineUser } from 'react-icons/ai';
import UserContext from '@stores/userProvider';
import Link from 'next/link';
import ModalAuthentication from '@com/Modal/ModalAuthentication';
import languagePage from '@asset/components/Layout/Header.json';
import { GetServerSideProps } from 'next';


interface Props {
	onClickOverlay: Function,
}

const Header: NextPage<Props> = ({ onClickOverlay }) => {
	const { notif } = useContext(NotifContext);
	const { locale, locales, asPath } = useRouter();
	const [profileBar, setProfileBar] = useState(false);
	const [languageBar, setLanguageBar] = useState<boolean>(false);
	const [t, setTranslation] = useState(languagePage[locale]);
	const refProfile = useRef<HTMLInputElement>();
	const refLanguage = useRef<HTMLInputElement>();
	const [showModal, setShowModal] = useState(false);

	const { mutate } = useMutation(() => Api.get('/sign-out'));
	const { user, setUser } = useContext(UserContext);

	const toogleModal = () => {
		setShowModal(!showModal);
	};

	const handleLogout = () => {
		setProfileBar(false);
		mutate(null, {
			onSuccess: (res) => {
				setUser(null);
				notif.success('Logout Successfully');
			},
			onError: (res) => {
				notif.error('Please cek you connection');
			}
		});
	};

	useEffect(() => {
		// setProfileBar(!profileBar)
		const checkIfClickedOutside = e => {
			// If the menu is open and the clicked target is not within the menu,
			// then close the menu
			if (profileBar && refProfile.current && !refProfile.current.contains(e.target)) {
				setProfileBar(false);
			}
			if (languageBar && refLanguage.current && !refLanguage.current.contains(e.target)) {
				setLanguageBar(false);
			}
		};

		document.addEventListener('mousedown', checkIfClickedOutside);

		return () => {
			// Cleanup the event listener
			document.removeEventListener('mousedown', checkIfClickedOutside);
		};
	}, [profileBar, languageBar]);

	useEffect(() => {
		setLanguageBar(false);
		setTranslation(languagePage[locale]);
	}, [locale]);


	return (
		<div className={'mb-16'}>
			<nav className="top-0 fixed w-full flex items-center bg-white shadow h-16 z-10">
				<div className={'flex justify-between w-full items-center'}>
					<div className={'ml-2 flex items-center'}>
						<div className={'flex justify-center items-center w-12 h-12'} onClick={() => onClickOverlay()}>
							<GoThreeBars size={'2em'} />
						</div>
						<h1 className={'ml-2'}>{process.env.APP_NAME}</h1>
					</div>
					<ModalAuthentication show={showModal} onClickOverlay={toogleModal} />
					<div className="relative inline-block text-lef">
						<div className='flex'>
							<div className='' ref={refLanguage}>
								<div className="flex items-center mr-4" onClick={() => setLanguageBar(!languageBar)}>
									<div className={'uppercase h-10 flex items-center focus:outline-none'}>
										{locale}
									</div>
								</div>
								<div className={`${languageBar ? 'absolute right-4 mt-2 w-40 rounded-md origin-top-right shadow-lg bg-white ring-1 ring-black ring-opacity-5 focus:outline-none' : 'hidden'}`}>
									<div className="py-1" role="none">
										{locales.map((v, i) => {
											return (
												<Link href={asPath} locale={v} key={i} passHref>
													<div className={'text-gray-700 block px-4 py-2 text-sm uppercase'}>
														{v}
													</div>
												</Link>
											);
										})}
									</div>
								</div>
							</div>
							{user === null ? (
								<div className="flex items-center mr-4">
									{/* <Link href={'/sign-in'}> */}
									<button className={'px-0 sm:px-6 py-2 rounded-lg hover:text-green-400 capitalize'} onClick={toogleModal}>
										{t.signIn}
									</button>
									{/* </Link> */}
								</div>
							) : !user.userId ? (
								<></>
							) : (
								<>
									<div className='' ref={refProfile}>
										<div className="flex items-center mr-4" onClick={() => setProfileBar(!profileBar)}>
											<div className="w-10 h-10 bg-gray-300 flex justify-center items-center rounded-full focus:outline-none">
												<AiOutlineUser className={'font-bold'} size={'1.5em'} />
											</div>
										</div>
										<div className={`${profileBar ? 'absolute right-4 mt-2 w-56 rounded-md origin-top-right shadow-lg bg-white ring-1 ring-black ring-opacity-5 focus:outline-none' : 'hidden'}`}>
											<div className="py-1" role="none">
												<a href="#" className="text-gray-700 block px-4 py-2 text-sm capitalize">{t.accountSettings}</a>
												<a href="#" className="text-gray-700 block px-4 py-2 text-sm capitalize">{t.support}</a>
												<a href="#" className="text-gray-700 block px-4 py-2 text-sm capitalize">{t.license}</a>
												<button className="text-gray-700 block w-full text-left px-4 py-2 text-sm capitalize" onClick={() => handleLogout()}>
													{t.signOut}
												</button>
											</div>
										</div>
									</div>
								</>
							)}
						</div>
					</div>

				</div>
			</nav>
		</div>
	);
};


// export const getServerSideProps: GetServerSideProps = async (context) => {
// 	console.log('context ', context);
// 	const { kanji } = context.query;

// 	// const data = await Api.get('/kanji/' + kanji).then(res => res);

// 	// console.log('context ', context.req.headers.cookie);
// 	// console.log('kanji ', kanji);
// 	// console.log('data ', data);

// 	return {
// 		props: {
// 			qKanji: kanji
// 		}
// 	};
// };

export default Header;