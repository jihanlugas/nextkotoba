import { ReactNode, useContext, useEffect } from 'react';
import type { NextPage } from 'next';
import Head from 'next/head';
import Header from './Header';
import Sidebar from './Sidebar';
import { useState } from 'react';
import { useMutation, useQuery } from 'react-query';
import { Api } from '../../lib/Api';
import UserContext from '@stores/userProvider';


type Props = {

};

const Main: NextPage<Props> = ({ children }) => {
	const [show, setShow] = useState(false);
	const { user, setUser } = useContext(UserContext);

	const onClickOverlay = (isShow: boolean) => {
		if (isShow === undefined) {
			setShow(!show);
		} else {
			setShow(isShow);
		}
	};


	const { data, mutate, isLoading } = useMutation(() => Api.get('/user/me'));

	useEffect(() => {
		if (user === null || user.userId === 0) {
			mutate(null, {
				onSuccess: (res) => {
					if (res) {
						if (res.success) {
							setUser(res.payload);
						} else {
							if (res.payload && res.payload.forceLogout) {
								setUser(null);
							}
						}
					}
				}
			});
		}
	}, []);

	return (
		<main className={''}>
			<Sidebar onClickOverlay={onClickOverlay} show={show} />
			<Header onClickOverlay={onClickOverlay} />
			{children}
		</main>
	);
};

export default Main;