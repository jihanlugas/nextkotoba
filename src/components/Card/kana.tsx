import { isEmptyObject } from '@utils/Validate';
import React from 'react';

interface Kana {
}

interface Card {
	primary: string,
	secondary: string,
	romaji: string,
}

const data = [
	{ hiragana: 'あ', katakana: 'ア', romaji: 'a', },
	{ hiragana: 'い', katakana: 'イ', romaji: 'i', },
	{ hiragana: 'う', katakana: 'ウ', romaji: 'u', },
	{ hiragana: 'え', katakana: 'エ', romaji: 'e', },
	{ hiragana: 'お', katakana: 'オ', romaji: 'o', },
	{ hiragana: 'か', katakana: 'カ', romaji: 'ka', },
	{ hiragana: 'き', katakana: 'キ', romaji: 'ki', },
	{ hiragana: 'く', katakana: 'ク', romaji: 'ku', },
	{ hiragana: 'け', katakana: 'ケ', romaji: 'ke', },
	{ hiragana: 'こ', katakana: 'コ', romaji: 'ko', },
	{ hiragana: 'が', katakana: 'ガ', romaji: 'ga', },
	{ hiragana: 'ぎ', katakana: 'ギ', romaji: 'gi', },
	{ hiragana: 'ぐ', katakana: 'グ', romaji: 'gu', },
	{ hiragana: 'げ', katakana: 'ゲ', romaji: 'ge', },
	{ hiragana: 'ご', katakana: 'ゴ', romaji: 'go', },
	{ hiragana: 'さ', katakana: 'サ', romaji: 'sa', },
	{ hiragana: 'し', katakana: 'シ', romaji: 'shi', },
	{ hiragana: 'す', katakana: 'ス', romaji: 'su', },
	{ hiragana: 'せ', katakana: 'セ', romaji: 'se', },
	{ hiragana: 'そ', katakana: 'ソ', romaji: 'so', },
	{ hiragana: 'ざ', katakana: 'ザ', romaji: 'za', },
	{ hiragana: 'じ', katakana: 'ジ', romaji: 'zi', },
	{ hiragana: 'ず', katakana: 'ズ', romaji: 'zu', },
	{ hiragana: 'ぜ', katakana: 'ゼ', romaji: 'ze', },
	{ hiragana: 'ぞ', katakana: 'ゾ', romaji: 'zo', },
	{ hiragana: 'た', katakana: 'タ', romaji: 'ta', },
	{ hiragana: 'ち', katakana: 'チ', romaji: 'chi', },
	{ hiragana: 'つ', katakana: 'ツ', romaji: 'tsu', },
	{ hiragana: 'て', katakana: 'テ', romaji: 'te', },
	{ hiragana: 'と', katakana: 'ト', romaji: 'to', },
	{ hiragana: 'だ', katakana: 'ダ', romaji: 'da', },
	{ hiragana: 'ぢ', katakana: 'ヂ', romaji: 'dji', },
	{ hiragana: 'づ', katakana: 'ヅ', romaji: 'dzu', },
	{ hiragana: 'で', katakana: 'デ', romaji: 'de', },
	{ hiragana: 'ど', katakana: 'ド', romaji: 'do', },
	{ hiragana: 'な', katakana: 'ナ', romaji: 'na', },
	{ hiragana: 'に', katakana: 'ニ', romaji: 'ni', },
	{ hiragana: 'ぬ', katakana: 'ヌ', romaji: 'nu', },
	{ hiragana: 'ね', katakana: 'ネ', romaji: 'ne', },
	{ hiragana: 'の', katakana: 'ノ', romaji: 'no', },
	{ hiragana: 'は', katakana: 'ハ', romaji: 'ha', },
	{ hiragana: 'ひ', katakana: 'ヒ', romaji: 'hi', },
	{ hiragana: 'ふ', katakana: 'フ', romaji: 'hu', },
	{ hiragana: 'へ', katakana: 'ヘ', romaji: 'he', },
	{ hiragana: 'ほ', katakana: 'ホ', romaji: 'ho', },
	{ hiragana: 'ば', katakana: 'バ', romaji: 'ba', },
	{ hiragana: 'び', katakana: 'ビ', romaji: 'bi', },
	{ hiragana: 'ぶ', katakana: 'ブ', romaji: 'bu', },
	{ hiragana: 'べ', katakana: 'ベ', romaji: 'be', },
	{ hiragana: 'ぼ', katakana: 'ボ', romaji: 'bo', },
	{ hiragana: 'ぱ', katakana: 'パ', romaji: 'pa', },
	{ hiragana: 'ぴ', katakana: 'ピ', romaji: 'pi', },
	{ hiragana: 'ぷ', katakana: 'プ', romaji: 'pu', },
	{ hiragana: 'ぺ', katakana: 'ペ', romaji: 'pe', },
	{ hiragana: 'ぽ', katakana: 'ポ', romaji: 'po', },
	{ hiragana: 'ま', katakana: 'マ', romaji: 'ma', },
	{ hiragana: 'み', katakana: 'ミ', romaji: 'mi', },
	{ hiragana: 'む', katakana: 'ム', romaji: 'mu', },
	{ hiragana: 'め', katakana: 'メ', romaji: 'me', },
	{ hiragana: 'も', katakana: 'モ', romaji: 'mo', },
	{ hiragana: 'や', katakana: 'ヤ', romaji: 'ya', },
	{},
	{ hiragana: 'ゆ', katakana: 'ユ', romaji: 'yu', },
	{},
	{ hiragana: 'よ', katakana: 'ヨ', romaji: 'yo', },
	{ hiragana: 'ら', katakana: 'ラ', romaji: 'ra', },
	{ hiragana: 'り', katakana: 'リ', romaji: 'ri', },
	{ hiragana: 'る', katakana: 'ル', romaji: 'ru', },
	{ hiragana: 'れ', katakana: 'レ', romaji: 're', },
	{ hiragana: 'ろ', katakana: 'ロ', romaji: 'ro', },
	{ hiragana: 'わ', katakana: 'ワ', romaji: 'wa', },
	{},
	{},
	{},
	{ hiragana: 'を', katakana: 'ヲ', romaji: 'wo', },
	{ hiragana: 'ん', katakana: 'ン', romaji: 'n', },
];

const Kana: React.FC<Kana> = () => {


	const Card: React.FC<Card> = ({ primary, secondary, romaji }) => {
		return (
			<div className={'animate-fadeIn shadow-md bg-white rounded-sm font-notoSerif'}>
				<div className={'flex justify-center items-center pt-2 mb-2 font-semibold'}>
					<h2 className={'text-3xl'}>{primary}</h2>
				</div>
				<div className={'flex justify-center items-center'}>
					<div className={'flex justify-around items-center w-full mb-2'}>
						<span className={'font-semibold'}>{secondary}</span>
						<span className={'font-semibold'}>{romaji}</span>
					</div>
				</div>
			</div>
		);
	};

	return (
		<>
			<div className={'grid grid-cols-5 gap-2 sm:gap-4'}>
				{data.map((kana, key) => {
					const primary = kana.hiragana;
					const secondary = kana.katakana;
					const romaji = kana.romaji;

					if (isEmptyObject(kana)) {
						return (
							<div key={key} className={'flex'} />
						);
					} else {
						return (
							<Card key={key} primary={primary} secondary={secondary} romaji={romaji} />
						);
					}
				})}
			</div>
		</>
	);
};

export default Kana;