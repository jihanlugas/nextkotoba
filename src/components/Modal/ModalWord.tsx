import React from 'react';
import { useRouter } from 'next/router';
import AppContext from '@stores/notifContext';
import * as Yup from 'yup';
import { Form, Formik, FormikValues, FieldArray } from 'formik';
import { useMutation } from 'react-query';
import Modal from '@com/Modal/Modal';
import TextField from '@com/Formik/TextField';
import TextAreaField from '@com/Formik/TextAreaField';
import ButtonSubmit from '@com/Formik/ButtonSubmit';
import { Api } from '@lib/Api';
import { AiOutlineLoading3Quarters } from 'react-icons/ai';
import { BsTrash } from 'react-icons/bs';
import NotifContext from '@stores/notifContext';
import languagePage from '@asset/components/Modal/ModalWord.json';
import languageForm from '@asset/form.json';

interface Props {
	show: boolean;
	onClickOverlay: (id?: number, refresh?: boolean) => void;
	selectedId: number;
}

type notif = {
	success: (msg: string) => void;
	error: (msg: string) => void;
	info: (msg: string) => void;
	warning: (msg: string) => void;
}

interface Init {
	wordId: number,
	word: string,
	kana: string,
	means: string,
	description: string,
}



const initFormikValue = {
	wordId: 0,
	word: '',
	kana: '',
	mean: '',
	description: '',
};

const ModalWord: React.FC<Props> = ({ show, onClickOverlay, selectedId = 0 }) => {
	const { locale } = useRouter();
	const { notif } = React.useContext(NotifContext);
	const { data, mutate, isLoading } = useMutation((val: FormikValues) => Api.post('/msword/create', val));
	const tForm = languageForm[locale];
	const tPage = languagePage[locale];

	const schema = Yup.object().shape({
		word: Yup.string().required(tForm.yup.required),
		kana: Yup.string().required(tForm.yup.required).matches(/^[ぁ-んァ-ン.。\s|-]+$/, 'field kana'),
		mean: Yup.string().required(tForm.yup.required),
		description: Yup.string(),
	});

	const handleSubmit = (values: FormikValues, { setErrors, resetForm }) => {
		mutate(values, {
			onSuccess: (res) => {
				if (res) {
					if (res.success) {
						onClickOverlay(0, true);
						notif.success(res.message);
						resetForm();
					} else if (res.error) {
						if (res.payload && res.payload.listError) {
							setErrors(res.payload.listError);
						} else {
							notif.error(res.message);
						}
					}
				}
			},
			onError: (res) => {
				notif.error('Please cek you connection');
			}
		});
	};

	return (
		<Modal show={show} onClickOverlay={onClickOverlay}>
			<div className={'p-4'}>
				<div>
					<div className={'flex justify-start mb-4'}>
						<span className={'text-xl'}>{selectedId === 0 ? tPage.createWord : tPage.updateWord}</span>
					</div>
					<Formik
						initialValues={initFormikValue}
						validationSchema={schema}
						enableReinitialize={true}
						onSubmit={handleSubmit}
					>
						{({ values }) => {
							return (
								<Form>
									<div className={''}>
										<div className="mb-4">
											<TextField
												label={tForm.label.word}
												name={'word'}
												type={'text'}
												placeholder={tForm.label.word}
												required
											/>
										</div>
										<div className="mb-4">
											<TextField
												label={tForm.label.kana}
												type={'text'}
												name={'kana'}
												placeholder={tForm.label.kana}
												required
											/>
										</div>
										<div className="mb-4">
											<TextField
												label={tForm.label.mean}
												type={'text'}
												name={'mean'}
												placeholder={tForm.label.mean}
												required
											/>
										</div>
										<div className="mb-4">
											<TextAreaField
												label={tForm.label.description}
												name={'description'}
												type={'text'}
												placeholder={tForm.label.description}
											/>
										</div>
										<div className={''}>
											<ButtonSubmit
												label={tForm.label.save}
												disabled={isLoading}
												loading={isLoading}
											/>
										</div>
									</div>
								</Form>
							);
						}}
					</Formik>
				</div>
			</div>
		</Modal>
	);
};

export default ModalWord;