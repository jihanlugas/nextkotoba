import React, { Dispatch, SetStateAction } from 'react';
import Modal from '@com/Modal/Modal';
import * as Yup from 'yup';
import { Form, Formik, FormikHelpers, FormikValues } from 'formik';
import { useMutation } from 'react-query';
import TextField from '@com/Formik/TextField';
import ButtonSubmit from '@com/Formik/ButtonSubmit';
import { Api } from '@lib/Api';
import UserContext from '@stores/userProvider';
import NotifContext from '@stores/notifContext';
import languagePage from '@asset/components/Modal/ModalAuthentication.json';
import languageForm from '@asset/form.json';
import { useRouter } from 'next/router';

interface Props {
	show: boolean;
	onClickOverlay: Function;
}

type notif = {
	success: (msg: string) => void;
	error: (msg: string) => void;
	info: (msg: string) => void;
	warning: (msg: string) => void;
}

interface PropsSignIn {
	t: any;
	onClickOverlay: Function;
	notif: notif;
	setPage: Dispatch<SetStateAction<string>>
}

interface PropsSignUp {
	t: any;
	onClickOverlay: Function;
	notif: notif;
	setPage: Dispatch<SetStateAction<string>>
}

const ModalAuthentication: React.FC<Props> = ({ show, onClickOverlay }) => {
	const { locale } = useRouter();
	const [page, setPage] = React.useState('sign-in');
	const { notif } = React.useContext(NotifContext);
	const t = {
		page: languagePage[locale],
		form: languageForm[locale],
	};

	return (
		<Modal show={show} onClickOverlay={onClickOverlay}>
			<div className={'p-4'}>
				<div>
					{page === 'sign-in' ? (
						<SignIn t={t} onClickOverlay={onClickOverlay} notif={notif} setPage={setPage} />
					) : (
						<SignUp t={t} onClickOverlay={onClickOverlay} notif={notif} setPage={setPage} />
					)}
				</div>
			</div>
		</Modal>
	);
};

const SignIn: React.FC<PropsSignIn> = ({ t, onClickOverlay, notif, setPage }) => {
	const { form: tForm, page: tPage } = t;
	const { user, setUser } = React.useContext(UserContext);

	const schema = Yup.object().shape({
		username: Yup.string().required(tForm.yup.required),
		passwd: Yup.string().required(tForm.yup.required),
	});

	const initFormikValue = {
		username: '',
		passwd: '',
	};

	const { data, mutate, isLoading } = useMutation((val: FormikValues) => Api.post('/sign-in', val));

	const handleSubmit = (values: FormikValues, { setErrors, resetForm }) => {
		mutate(values, {
			onSuccess: (res) => {
				if (res) {
					if (res.success) {
						setUser(res.payload);
						onClickOverlay();
						notif.success(res.message);
						resetForm();
					} else if (res.error) {
						if (res.payload && res.payload.listError) {
							setErrors(res.payload.listError);
						} else {
							notif.error(res.message);
						}
					}
				}
			},
			onError: (res) => {
				notif.error('Please cek you connection');
			}
		});
	};

	return (
		<>
			<div className={'flex justify-center'}>
				<span className={'text-xl'}>{tPage.signIn}</span>
			</div>
			<div className={'mb-2'}>
				<Formik
					initialValues={initFormikValue}
					validationSchema={schema}
					enableReinitialize={true}
					onSubmit={handleSubmit}
				>
					{() => {
						return (
							<Form>
								<div className={''}>
									<div className="mb-4">
										<TextField
											label={tForm.label.username}
											name={'username'}
											type={'text'}
											placeholder={tForm.label.username}
											required
										/>
									</div>
									<div className="mb-4">
										<TextField
											label={tForm.label.password}
											type={'password'}
											name={'passwd'}
											placeholder={tForm.label.password}
											autoComplete={'off'}
											required
										/>
									</div>
									<div className={''}>
										<ButtonSubmit
											label={tForm.label.signIn}
											disabled={isLoading}
											loading={isLoading}
										/>
									</div>
								</div>
							</Form>
						);
					}}
				</Formik>
			</div>
			<div className={'flex'}>
				<div className={'mr-1'}>
					{tPage.dontHaveAccount}
				</div>
				<button className={'text-blue-600'} onClick={() => setPage('sign-up')}>
					<div>{tPage.registerNow}</div>
				</button>
			</div>
		</>
	);
};

const SignUp: React.FC<PropsSignUp> = ({ t, onClickOverlay, notif, setPage }) => {
	const { form: tForm, page: tPage } = t;
	const schema = Yup.object().shape({
		email: Yup.string().email(tForm.yup.email).required(tForm.yup.required),
		fullname: Yup.string().required(tForm.yup.required),
		noHp: Yup.number().required(tForm.yup.required),
		username: Yup.string().trim().required(tForm.yup.required),
		passwd: Yup.string().required(tForm.yup.required),
		confirmPasswd: Yup.string().required(tForm.yup.required).oneOf([Yup.ref('passwd')], tForm.yup.matchPassword)
	});

	const initFormikValue = {
		email: '',
		fullname: '',
		noHp: '',
		username: '',
		passwd: '',
		confirmPasswd: '',
	};

	const { data, mutate, isLoading } = useMutation((val: FormikValues) => Api.post('/sign-up', val));

	const handleSubmit = (values: FormikValues, { setErrors, resetForm }) => {
		mutate(values, {
			onSuccess: (res) => {
				if (res) {
					if (res.success) {
						notif.success(res.message);
						resetForm();
					} else if (res.error) {
						if (res.payload && res.payload.listError) {
							setErrors(res.payload.listError);
						} else {
							notif.error(res.message);
						}
					}
				}
			},
			onError: (res) => {
				notif.error('Please cek you connection');
			}
		});
	};

	return (
		<>
			<div className={'flex justify-center'}>
				<span className={'text-xl'}>{tPage.signUp}</span>
			</div>
			<div className={'mb-2'}>
				<Formik
					initialValues={initFormikValue}
					validationSchema={schema}
					enableReinitialize={true}
					onSubmit={handleSubmit}
				>
					{() => {
						return (
							<Form>
								<div className={''}>
									<div className="mb-4">
										<TextField
											label={tForm.label.username}
											name={'username'}
											type={'text'}
											placeholder={tForm.label.username}
											required
										/>
									</div>
									<div className="mb-4">
										<TextField
											label={tForm.label.email}
											name={'email'}
											type={'text'}
											placeholder={tForm.label.email}
											required
										/>
									</div>
									<div className="mb-4">
										<TextField
											label={tForm.label.phoneNumber}
											name={'noHp'}
											type={'text'}
											placeholder={tForm.label.phoneNumber}
											required
										/>
									</div>
									<div className="mb-4">
										<TextField
											label={tForm.label.fullname}
											name={'fullname'}
											type={'text'}
											placeholder={tForm.label.fullname}
											required
										/>
									</div>
									<div className="mb-4">
										<TextField
											label={tForm.label.password}
											type={'password'}
											name={'passwd'}
											placeholder={tForm.label.password}
											required
										/>
									</div>
									<div className="mb-4">
										<TextField
											label={tForm.label.confirmPassword}
											type={'password'}
											name={'confirmPasswd'}
											placeholder={tForm.label.confirmPassword}
											required
										/>
									</div>
									<div className={''}>
										<ButtonSubmit
											label={tForm.label.signUp}
											disabled={isLoading}
											loading={isLoading}
										/>
									</div>
								</div>
							</Form>
						);
					}}
				</Formik>
			</div>
			<div className={'flex'}>
				<div className={'mr-1'}>
					{tPage.alreadyHaveAccount}
				</div>
				<button className={'text-blue-600'} onClick={() => setPage('sign-in')}>
					<div>{tPage.signInNow}</div>
				</button>
			</div>
		</>
	);
};

export default ModalAuthentication;