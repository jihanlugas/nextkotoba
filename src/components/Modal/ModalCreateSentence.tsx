import { useContext, useState } from 'react';
import { NextPage } from 'next';
import AppContext from '@stores/notifContext';
import * as Yup from 'yup';
import { Form, Formik, FormikValues } from 'formik';
import { useMutation } from 'react-query';
import Modal from '@com/Modal/Modal';
import TextField from '@com/Formik/TextField';
import TextAreaField from '@com/Formik/TextAreaField';
import ButtonSubmit from '@com/Formik/ButtonSubmit';
import { Api } from '@lib/Api';
import { useEffect } from 'react';
import { AiOutlineLoading3Quarters } from 'react-icons/ai';
import { BsTrash } from 'react-icons/bs';


interface Props {
	show: boolean;
	onClickOverlay: Function;
	selectedId: number;
}

interface Init {
	sentenceId: number,
	sentence: string,
	kana: string,
	mean: string,
	description: string,
}

let schema = Yup.object().shape({
	sentence: Yup.string().required(),
	kana: Yup.string().required(),
	mean: Yup.string().required(),
	description: Yup.string(),
});

const defaultInit = {
	sentenceId: 0,
	sentence: '',
	kana: '',
	mean: '',
	description: '',
};

const ModalCreateSentence: NextPage<Props> = ({ show, onClickOverlay, selectedId }) => {
	const { notif } = useContext(AppContext);
	const [init, setInit] = useState<Init>(defaultInit);
	const FormSave = useMutation((val: FormikValues) => Api.post('/sentence/form', val));
	const FormUpdate = useMutation((val: FormikValues) => Api.put('/sentence/update', val));
	const FormReq = useMutation((id: number) => Api.get('/sentence/' + id));
	const FormDelete = useMutation((id: number) => Api.delete('/sentence/' + id));

	const handleSubmit = (values: FormikValues, setErrors, resetForm) => {
		if (selectedId === 0) {
			FormSave.mutate(values, {
				onSuccess: (res) => {
					resetForm();
					setInit(defaultInit);
					notif.success(res.message);
					onClickOverlay(0, true);
				}
			});
		} else {
			FormUpdate.mutate(values, {
				onSuccess: (res) => {
					resetForm();
					setInit(defaultInit);
					notif.success(res.message);
					onClickOverlay(0, true);
				}
			});
		}
	};

	const handleDelete = (resetForm) => {
		FormDelete.mutate(selectedId, {
			onSuccess: (res) => {
				resetForm();
				setInit(defaultInit);
				notif.success(res.message);
				onClickOverlay(0, true);
			}
		});
	};

	useEffect(() => {
		if (selectedId === 0) {
			setInit(defaultInit);
		} else {
			FormReq.mutate(selectedId, {
				onSuccess: (res) => {
					setInit(res.payload);
				}
			});
		}
	}, [selectedId]);

	return (
		<Modal show={show} onClickOverlay={onClickOverlay}>
			<div className={'p-4'}>
				{FormReq.isLoading ? (
					<div className={'h-60 flex justify-center items-center'}>
						<AiOutlineLoading3Quarters className={'animate-spin'} size={'4em'} />
					</div>
				) : (
					<Formik
						initialValues={init}
						validationSchema={schema}
						enableReinitialize={true}
						onSubmit={(values, { setErrors, resetForm }) => handleSubmit(values, setErrors, resetForm)}
					>
						{({ resetForm }) => {
							return (
								<Form>
									<div>
										<div className={'flex justify-between items-center mb-4'}>
											<span className={'text-xl'}>Sentence</span>
											{selectedId !== 0 && (
												<div className={'flex justify-center items-center h-8 w-8'} onClick={() => (handleDelete(resetForm))} >
													<BsTrash className={'text-red-600'} size={'1em'} />
												</div>
											)}
										</div>
										<div className="mb-4">
											<TextField
												label={'Sentence'}
												name={'sentence'}
												type={'text'}
												placeholder={'Sentence'}
											/>
										</div>
										<div className="mb-4">
											<TextField
												label={'Kana'}
												name={'kana'}
												type={'text'}
												placeholder={'Kana'}
											/>
										</div>
										<div className="mb-4">
											<TextField
												label={'Mean'}
												name={'mean'}
												type={'text'}
												placeholder={'Mean'}
											/>
										</div>
										<div className="mb-4">
											<TextAreaField
												label={'Description'}
												name={'description'}
												type={'text'}
												placeholder={'Description'}
											/>
										</div>
										<div className={''}>
											<ButtonSubmit
												label={'Save'}
												disabled={FormSave.isLoading || FormUpdate.isLoading}
												loading={FormSave.isLoading || FormUpdate.isLoading}
											/>
										</div>
									</div>
								</Form>
							);
						}}
					</Formik>
				)}

			</div>
		</Modal>
	);
};

export default ModalCreateSentence;