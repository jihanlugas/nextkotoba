import { FastField, ErrorMessage } from 'formik';
import { NextPage } from 'next';

interface Props {
	label?: string;
	name: string;
	type: string;
	placeholder?: string;
	required?: boolean;
	autoComplete?: string;
	// props?: any;
}

const TextField: NextPage<Props> = ({ label, name, type, placeholder = 'Enter', ...props }) => {
	return (
		<div className={'flex flex-col w-full'}>
			{label && (
				<div className={''}>
					<span>{label}</span>
					{props.required && <span className={'text-red-600'}>{'*'}</span>}
				</div>
			)}
			<FastField
				className={'w-full border-2 rounded h-10 px-2 bg-gray-50'}
				type={type}
				name={name}
				placeholder={placeholder}
				{...props}
			/>
			<ErrorMessage name={name}>
				{(msg) => {
					return (
						<div className={'text-red-600 text-sm normal-case'}>{msg}</div>
					);
				}}
			</ErrorMessage>
		</div>
	);
};

export default TextField;