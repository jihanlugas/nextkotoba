import { Field, ErrorMessage } from 'formik';
import { NextPage } from 'next';

interface Props {
	label?: string;
	name: string;
	type: string;
	placeholder?: string;
	required?: boolean;
}

const TextAreaField: NextPage<Props> = ({ label, name, type, placeholder = 'Enter', required }) => {
	return (
		<div className={'flex flex-col w-full'}>
			{label && (
				<div className={''}>
					<span>{label}</span>
					{required && <span className={'text-red-600'}>{'*'}</span>}
				</div>
			)}
			<Field
				as={'textarea'}
				className={'w-full border-2 rounded h-20 px-2 bg-gray-50'}
				type={type}
				name={name}
				placeholder={placeholder}
			/>
			<ErrorMessage name={name}>
				{(msg) => {
					return (
						<div className={'text-red-600 text-sm normal-case'}>{msg}</div>
					);
				}}
			</ErrorMessage>
		</div>
	);
};

export default TextAreaField;