interface Props {
	kana?: string
	kanji?: string
	mean?: string
	description?: string
}

const Example: React.FC<Props> = ({ kana, kanji, mean, description }) => {
	return (
		<div className={'mb-2 p-2 border rounded'}>
			<ruby className={'text-lg font-bold font-notoSerif mb-2'}>
				{kanji}<rt>{kana}</rt>
			</ruby>
			<div className={'mb-2'}>
				{mean}
			</div>
			<div className={'whitespace-pre-wrap text-sm'}>
				{description}
			</div>

		</div>
	);
};

export default Example;