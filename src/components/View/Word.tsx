import Link from 'next/link';

interface Props {
	mswordId?: number,
	listMsword?: string[],
	word?: string,
	kana?: string,
	mean?: string,
	description?: string,
}

const Word: React.FC<Props> = ({ mswordId, listMsword, word, kana, mean, description }) => {
	return (
		<div className={'bg-white shadow-md rounded-md animate-fadeIn p-2 select-none font-notoSerif'}>
			<div className={'text-xl font-semibold mb-2'}>
				<ruby>
					{word}<rt className={''}>{kana}</rt>
				</ruby>
			</div>
			<div className=' mb-2'>
				{mean}
			</div>
			<div className={'whitespace-pre-wrap text-sm mb-2'}>
				{description}
			</div>
			<div className={'flex flex-wrap'}>
				{listMsword.map((kanji, key) => {
					return (
						<Link key={key} href={{ pathname: '/kanji/' + kanji }}>
							<a className={'h-10 w-10 flex justify-center items-center shadow-md rounded-md mr-4 text-lg font-semibold hover:bg-gray-200 '}>
								<span>{kanji}</span>
							</a>
						</Link>
					);
				})}
			</div>
		</div>
	);
};

export default Word;