interface Props {
	data?: string
}

const OnReading: React.FC<Props> = ({ data }) => {
	return (
		<div className={'px-2 bg-green-400 text-gray-50 mr-2 font-bold font-notoSerif rounded-md mb-2'}>
			{data}
		</div>
	);
};

export default OnReading;