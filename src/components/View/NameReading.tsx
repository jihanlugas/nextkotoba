interface Props {
	data?: string
}

const NameReading: React.FC<Props> = ({ data }) => {
	return (
		<div className={'px-2 bg-gray-700 text-gray-50 mr-2 font-bold font-notoSerif rounded-md mb-2'}>
			{data}
		</div>
	);
};

export default NameReading;