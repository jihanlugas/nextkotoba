import { createContext, useEffect, FC, ReactNode, useState, Dispatch, SetStateAction } from 'react';




interface Props {

}

type AppContextState = {
	email: string;
	username: string;
	fullname: string;
	userId: number;

}

const initUser = {
	email: '',
	username: '',
	fullname: '',
	userId: 0,
};

const UserContext = createContext({
	user: initUser,
	setUser: (state: AppContextState) => { },
});

export const UserContextProvider: FC<Props> = ({ children }) => {

	const [user, setUser] = useState<AppContextState>(initUser);

	const context = {
		user,
		setUser,
	};

	return (
		<UserContext.Provider value={context}>
			{children}
		</UserContext.Provider>
	);
};

export default UserContext;