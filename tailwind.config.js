module.exports = {
	// purge: [],
	// darkMode: false, // or 'media' or 'class'
	content: [
		'./src/pages/**/*.{js,ts,jsx,tsx}',
		'./src/components/**/*.{js,ts,jsx,tsx}',
		'./src/stores/**/*.{js,ts,jsx,tsx}',
	],
	theme: {
		extend: {
			keyframes: {
				fadeIn: {
					'0%': {
						opacity: 0,
						transform: 'translateX(-0.5rem)',
					},
					'100%': {
						opacity: 1,
						transform: 'translateX(0)',
					},
				},
			},
			animation: {
				fadeIn: 'fadeIn 700ms'
			},
			// transitionProperty: {
			// 	'height': 'height',
			// },
			colors: {
				'orange': {
					'50': '#fff7ed',
					'100': '#ffedd5',
					'200': '#fed7aa',
					'300': '#fdba74',
					'400': '#fb923c',
					'500': '#f97316',
					'600': '#ea580c',
					'700': '#c2410c',
					'800': '#9a3412',
					'900': '#7c2d12'
				},
			}
		},
		fontFamily: {
			notoSerif: ['Noto Serif JP', 'serif'],
			ubuntu: ['Ubuntu', 'sans-serif'],
			yujiSyuku: ['Yuji Syuku', 'serif'],
		},
	},
	plugins: [],
};
