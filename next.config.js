/** @type {import('next').NextConfig} */
module.exports = {
	reactStrictMode: true,
	env: {
		APP_NAME: process.env.APP_NAME,
		API_END_POINT: process.env.API_END_POINT,
		APP_ICON_32: process.env.APP_ICON_32,
		APP_ICON_192: process.env.APP_ICON_192,
		APP_ICON_512: process.env.APP_ICON_512,
		COOKIE_NAME: process.env.COOKIE_NAME,
	},
	i18n: {
		// providing the locales supported by your application
		locales: ['en', 'id'],
		//  default locale used when the non-locale paths are visited
		defaultLocale: 'en',
	},
};
